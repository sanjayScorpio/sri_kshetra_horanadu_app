import 'package:flutter/material.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

final FocusNode nodeText1 = FocusNode();

final FocusNode nodeText2 = FocusNode();

final FocusNode nodeText3 = FocusNode();

final FocusNode nodeText4 = FocusNode();

final FocusNode nodeText5 = FocusNode();

final FocusNode nodeText6 = FocusNode();

final FocusNode nodeText7 = FocusNode();

final FocusNode nodeText8 = FocusNode();

final FocusNode nodeText9 = FocusNode();

final FocusNode nodeText10 = FocusNode();

KeyboardActionsConfig buildConfig(BuildContext context) {
  return KeyboardActionsConfig(
    keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
    keyboardBarColor: Colors.grey[200],
    nextFocus: true,
    actions: [
      KeyboardActionsItem(
        focusNode: nodeText1,
        displayArrows: true,
      ),
      // KeyboardActionsItem(focusNode: nodeText2, toolbarButtons: [
      //   (node) {
      //     return GestureDetector(
      //       onTap: () => node.unfocus(),
      //       child: Padding(
      //         padding: EdgeInsets.all(8.0),
      //         child: Icon(Icons.close),
      //       ),
      //     );
      //   }
      // ]),
      KeyboardActionsItem(
        focusNode: nodeText2,
        displayDoneButton: true,
        displayArrows: true,
      ),
      KeyboardActionsItem(
        focusNode: nodeText3,
        displayDoneButton: true,
        displayArrows: true,
      ),
      KeyboardActionsItem(
        focusNode: nodeText4,
        displayArrows: true,
      ),
      KeyboardActionsItem(
        focusNode: nodeText5,
        displayArrows: true,
      ),
      KeyboardActionsItem(
        focusNode: nodeText6,
        displayArrows: true,
      ),
      KeyboardActionsItem(
        focusNode: nodeText7,
        displayArrows: true,
      ),
      KeyboardActionsItem(
        focusNode: nodeText8,
        displayArrows: true,
      ),
      KeyboardActionsItem(
        focusNode: nodeText9,
        displayArrows: true,
      ),
      KeyboardActionsItem(
        focusNode: nodeText10,
        displayArrows: false,
      ),
    ],
  );
}
