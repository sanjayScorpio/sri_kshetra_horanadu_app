Future<bool> delay(Duration duration) {
  return Future<bool>.delayed(duration, () => true);
}
