import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class DatePickerButtonWhite extends StatefulWidget {
  final double textBoxWidth;
  final String text;
  DatePickerButtonWhite({
    Key key,
    this.textBoxWidth,
    this.text,
  }) : super(key: key);

  @override
  _DatePickerButtonWhiteState createState() => _DatePickerButtonWhiteState();
}

class _DatePickerButtonWhiteState extends State<DatePickerButtonWhite> {
  DateTime _date;

  Future<Null> _selectedDate(BuildContext context) async {
    final DateTime datePicker = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime.now(),
      errorFormatText: 'Enter valid date',
      fieldLabelText: 'Order date',
      errorInvalidText: 'Enter date in valid range',
    );

    if (datePicker != null && datePicker != _date) {
      print("Date selected ${_date.toString()}");
      setState(() {
        _date = datePicker;
      });
    } else {
      _date = DateTime.now();
    }
  }

  @override
  Widget build(BuildContext context) {
    // double textBoxWidth =
    //     (MediaQuery.of(context).size.width / 2).toDouble() - 50;
    return GestureDetector(
      onTap: () => _selectedDate(context),
      child: Container(
        width: widget.textBoxWidth,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: ThemeColors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                _date == null
                    ? widget.text
                    : DateFormat('dd-MM-yyyy').format(_date),
                style: ThemeText.generalButtonTextPrimary,
              ),
              Icon(
                MdiIcons.calendarOutline,
                color: ThemeColors.primary,
                size: 18,
              )
            ],
          ),
        ),
      ),
    );
  }
}
