import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class CustomerDetailsCard extends StatelessWidget {
  final IconData icon;
  final Color iconBG;
  final String title;
  final String value;
  CustomerDetailsCard({
    this.icon,
    this.iconBG,
    this.title,
    this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: ThemeColors.white, borderRadius: BorderRadius.all(Radius.circular(15))),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 55,
              height: 55,
              child: Icon(
                icon,
                size: 30,
                color: ThemeColors.white,
              ),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: iconBG,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: ThemeText.generalDescriptionLight,
                    ),
                    SizedBox(
                      height: 1,
                    ),
                    Text(
                      value,
                      style: ThemeText.profileDisplayText,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      maxLines: 4,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
