import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class PrimaryHeaderContainer extends StatelessWidget {
  final Widget child;
  PrimaryHeaderContainer({this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: ThemeColors.secondary,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30.0),
          bottomRight: Radius.circular(30.0),
        ),
      ),
      child: Stack(
        children: [
          Positioned(
            top: 10,
            left: 100,
            child: Image.asset("assets/images/dots.png"),
          ),
          Positioned(
            top: -120.0,
            right: -100.0,
            child: Image.asset("assets/images/rect_right.png"),
          ),
          Container(
            child: this.child,
          ),
        ],
      ),
    );
  }
}
