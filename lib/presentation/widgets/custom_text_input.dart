import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class CustomTextInput extends StatefulWidget {
  final String labelText;
  final String hint;
  final Widget prefix;
  final TextInputType keyboardType;
  final TextEditingController controller;
  final int maxlines;
  final IconData prefixIcon;
  final bool suffixIcon;
  final String Function(String) validator;
  final List<TextInputFormatter> inputFormatters;
  final TextInputAction textInputAction;
  final TextStyle style;
  final TextAlign textAlign;
  final FocusNode focusNode;
  final bool showText;
  CustomTextInput({
    Key key,
    this.labelText,
    this.hint,
    this.prefix,
    this.keyboardType,
    this.controller,
    this.maxlines: 1,
    this.prefixIcon,
    this.suffixIcon,
    this.validator,
    this.inputFormatters,
    this.textInputAction,
    this.style,
    this.textAlign: TextAlign.left,
    this.showText: false,
    this.focusNode,
  }) : super(key: key);

  @override
  _CustomTextInputState createState() => _CustomTextInputState();
}

class _CustomTextInputState extends State<CustomTextInput> {
  bool _obscureText = false;

  @override
  void initState() {
    _obscureText = widget.showText;

    super.initState();
  }

  void _togglePasswordEye() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.labelText != null) ...{
          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Text(
              widget.labelText,
              style: ThemeText.generalTextBold,
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
        },
        TextFormField(
          textAlign: widget.textAlign,
          textInputAction: widget.textInputAction,
          validator: widget.validator,
          controller: widget.controller,
          keyboardType: widget.keyboardType,
          focusNode: widget.focusNode,
          inputFormatters: widget.inputFormatters,
          // style: widget.style != null ? widget.style : ThemeText.textInput,
          style: ThemeText.textInput,
          maxLines: widget.maxlines,
          obscureText: _obscureText,
          decoration: InputDecoration(
            suffixIcon: widget.showText == true
                ? GestureDetector(
                    onTap: _togglePasswordEye,
                    child: Icon(
                      _obscureText
                          ? MdiIcons.eyeOutline
                          : MdiIcons.eyeOffOutline,
                      color: ThemeColors.lightGrey,
                    ),
                  )
                : null,
            // prefixIcon: widget.prefixIcon != null
            //     ? Icon(
            //         widget.prefixIcon,
            //         color: ThemeColors.secondary,
            //       )
            //     : null,
            prefix: widget.prefix,
            prefixStyle: ThemeText.textInput,
            //prefixStyle: ThemeText.textInput,
            contentPadding: widget.prefix != null
                ? EdgeInsets.fromLTRB(20.0, 10.0, 15.0, 10.0)
                : EdgeInsets.all(15.0),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: ThemeColors.secondary,
                width: 2.0,
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              ),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(18.0),
              ),
              borderSide: BorderSide(
                color: ThemeColors.lightGrey,
                width: 2.0,
              ),
            ),
            //  filled: true,
            hintStyle: TextStyle(
              color: ThemeColors.lightGrey,
            ),
            hintText: widget.hint,
            //  fillColor: Colors.white70,
          ),
        ),
      ],
    );
  }
}
