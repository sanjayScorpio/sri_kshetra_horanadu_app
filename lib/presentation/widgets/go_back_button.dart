import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class GoBackButton extends StatelessWidget {
  const GoBackButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: GestureDetector(
      onTap: () => {Navigator.pop(context)},
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(
            MdiIcons.chevronLeft,
            color: ThemeColors.secondaryDark,
            size: 30,
          ),
          Text(
            "Back",
            style: ThemeText.backButtonText,
          )
        ],
      ),
    ));
  }
}
