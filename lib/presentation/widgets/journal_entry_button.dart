import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class JournalButton extends StatelessWidget {
  final String title;
  final Function onPress;
  final IconData icon;
  const JournalButton({
    Key key,
    @required this.title,
    @required this.onPress,
    @required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: onPress,
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.all(15),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: ThemeColors.primary,
            borderRadius: BorderRadius.circular(22.0),
          ),
          child: Row(
            children: [
              Icon(
                icon,
                color: ThemeColors.white,
                size: 30,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 5.0, 0.0, 5.0),
                  child: Text(
                    this.title,
                    style: ThemeText.generalButtonText,
                  ),
                ),
              ),
              Icon(
                MdiIcons.chevronRight,
                color: ThemeColors.white,
                size: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
