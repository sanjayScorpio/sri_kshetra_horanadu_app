import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class CustomToggleButtons extends StatelessWidget {
  final String text;
  final bool isActive;
  final Function onPress;
  CustomToggleButtons(this.text, this.isActive, this.onPress);

  _active() {
    return BoxDecoration(
        gradient: ThemeGradients.linearGradientVertical,
        borderRadius: BorderRadius.circular(12));
  }

  _inactive() {
    return BoxDecoration(
        color: ThemeColors.lightBlue, borderRadius: BorderRadius.circular(12));
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        decoration: isActive ? _active() : _inactive(),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Text(
              text,
              style: isActive
                  ? ThemeText.toggleButtonTextlight
                  : ThemeText.toggleButtonTextDark,
            ),
          ),
        ),
      ),
    );
  }
}
