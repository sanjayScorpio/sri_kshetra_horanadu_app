import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class EditItemCard extends StatefulWidget {
  final Function deleteItem;
  EditItemCard({Key key, this.deleteItem}) : super(key: key);

  @override
  _EditItemCardState createState() => _EditItemCardState();
}

class _EditItemCardState extends State<EditItemCard> {
  String amount;
  TextEditingController quantityController = TextEditingController(text: '1');
  TextEditingController rateController = TextEditingController(text: '0');

  @override
  void initState() {
    handleRates();
    super.initState();
  }

  void handleRates() {
    amount = (double.parse(quantityController.text) *
            double.parse(rateController.text))
        .toStringAsFixed(2)
        .toString();
  }

  @override
  void dispose() {
    quantityController.dispose();
    rateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * .15,
                height: MediaQuery.of(context).size.width * .15,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      'https://i.pinimg.com/originals/08/c1/56/08c156281d64773efbc1811ec74c875f.jpg',
                    ),
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.0),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Stack(
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 10,
                                ),
                                child: Text(
                                  StringUtils.capitalize('Puppy matches'),
                                  style: ThemeText.firmName,
                                  overflow: TextOverflow.fade,
                                  maxLines: 1,
                                ),
                              ),
                              Positioned(
                                right: 0,
                                bottom: 1,
                                top: 1,
                                child: IconButton(
                                  onPressed: widget.deleteItem,
                                  icon: Icon(MdiIcons.deleteOutline),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          GradientConatiner(
                            child: Text(
                              StringUtils.capitalize("#45354"),
                              style: ThemeText.signOutWhite,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          GradientConatiner(
                            child: Text(
                              StringUtils.capitalize("firm"),
                              style: ThemeText.signOutWhite,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          GradientConatiner(
                            child: Text(
                              StringUtils.capitalize("GST - (18%)"),
                              style: ThemeText.signOutWhite,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "Qty (Kg)",
                      style: ThemeText.itemTitleText,
                    ),
                    TextFormField(
                      style: ThemeText.textInput,
                      controller: quantityController,
                      keyboardType:
                          TextInputType.numberWithOptions(decimal: true),
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(
                          RegExp(r'^\d+\.?\d{0,2}'),
                        ),
                      ],
                      onChanged: (value) {
                        handleRates();
                      },
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(0, 5, 5, 3),
                        isDense: true,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Rate (₹ / Kg)",
                      style: ThemeText.itemTitleText,
                    ),
                    TextFormField(
                      style: ThemeText.textInput,
                      controller: rateController,
                      onChanged: (value) {
                        handleRates();
                      },
                      keyboardType:
                          TextInputType.numberWithOptions(decimal: true),
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(
                          RegExp(r'^\d+\.?\d{0,2}'),
                        ),
                      ],
                      maxLines: 1,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(0, 5, 5, 3),
                        isDense: true,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "Amount (₹)",
                      style: ThemeText.itemTitleText,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      amount,
                      style: ThemeText.itemValueText,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
