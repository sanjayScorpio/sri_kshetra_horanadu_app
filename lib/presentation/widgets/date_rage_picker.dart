import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class DateRangePicker extends StatefulWidget {
  final double textBoxWidth;
  final String text;
  DateRangePicker({
    Key key,
    this.textBoxWidth,
    this.text,
  }) : super(key: key);

  @override
  _DateRangePickerState createState() => _DateRangePickerState();
}

class _DateRangePickerState extends State<DateRangePicker> {
  DateTime _date;
  DateTimeRange datePicker = DateTimeRange(
    start: DateTime(2015),
    end: DateTime.now(),
  );

  Future<Null> _selectedDate(BuildContext context) async {
    datePicker = await showDateRangePicker(
      context: context,
      firstDate: DateTime(2015),
      lastDate: DateTime.now(),
      initialDateRange: DateTimeRange(
        start: DateTime(2015),
        end: DateTime.now(),
      ),
      errorFormatText: 'Enter valid date',
      errorInvalidText: 'Enter date in valid range',
    );

    if (datePicker != null && datePicker != _date) {
      // print("Date selected ${_date.toString()}");
      print("Date selected $datePicker");
      print("Date selected ${datePicker.start}");
      print("Date selected ${datePicker.end}");
      setState(() {
        // _date = datePicker;
      });
    } else {
      print("Date selected $datePicker");
      _date = DateTime.now();
    }
  }

  @override
  Widget build(BuildContext context) {
    // double textBoxWidth =
    //     (MediaQuery.of(context).size.width / 2).toDouble() - 50;
    return Column(
      children: [
        GestureDetector(
          onTap: () => _selectedDate(context),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: ThemeColors.darkGrey.withOpacity(0.1),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          "From",
                          style: ThemeText.generalDescriptionBold,
                        ),
                        Text(
                          DateFormat('dd-MM-yyyy').format(datePicker.start),
                          style: ThemeText.orderTotalAmount,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          "To",
                          style: ThemeText.generalDescriptionBold,
                        ),
                        Text(
                          DateFormat('dd-MM-yyyy').format(datePicker.end),
                          style: ThemeText.orderTotalAmount,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        // SizedBox(
        //   height: 15,
        // ),
        // GestureDetector(
        //   onTap: () => _selectedDate(context),
        //   child: Text(
        //     "Select date range",
        //     style: ThemeText.selectDateRange,
        //   ),
        // ),
        // GestureDetector(
        //   onTap: () => _selectedDate(context),
        //   child: Container(
        //     width: widget.textBoxWidth,
        //     decoration: BoxDecoration(
        //       borderRadius: BorderRadius.circular(15),
        //       color: ThemeColors.white,
        //     ),
        //     child: Padding(
        //       padding:
        //           const EdgeInsets.symmetric(vertical: 12, horizontal: 15.0),
        //       child: Row(
        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         children: [
        //           Text(
        //             _date == null
        //                 ? widget.text
        //                 : DateFormat('dd-MM-yyyy').format(_date),
        //             style: ThemeText.generalButtonTextPrimary,
        //           ),
        //           Icon(
        //             MdiIcons.calendarOutline,
        //             color: ThemeColors.primary,
        //             size: 18,
        //           )
        //         ],
        //       ),
        //     ),
        //   ),
        // ),
      ],
    );
  }
}
