import 'package:flutter/cupertino.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class StatisticsWidet extends StatelessWidget {
  final String slips;
  final String orders;
  final Function slipsOnPress;
  final Function pendingOnPress;
  final Function ordersOnPress;
  final String pending;

  StatisticsWidet({
    this.orders,
    this.pending,
    this.slips,
    this.slipsOnPress,
    this.pendingOnPress,
    this.ordersOnPress,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        color: ThemeColors.white,
          boxShadow: [
            BoxShadow(
              color: ThemeColors.shadowGrey,
              offset: Offset(2, 0),
              blurRadius: 10.0,
            ),
          ],
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: slipsOnPress,
              child: Column(
                children: [
                  Text(
                    slips,
                    style: ThemeText.statsTextGreen,
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    "Active Slips",
                    style: ThemeText.statsDesc,
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.center,
              height: 60,
              width: 2,
              decoration: BoxDecoration(
                color: ThemeColors.lightGrey.withOpacity(0.3),
              ),
            ),
            GestureDetector(
              onTap: pendingOnPress,
              child: Column(
                children: [
                  Text(
                    pending,
                    style: ThemeText.statsTextRed,
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    "Pending Amount",
                    style: ThemeText.statsDesc,
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.center,
              height: 60,
              width: 2,
              decoration: BoxDecoration(
                color: ThemeColors.lightGrey.withOpacity(0.3),
              ),
            ),
            GestureDetector(
              onTap: ordersOnPress,
              child: Column(
                children: [
                  Text(
                    orders,
                    style: ThemeText.statsTextOrrange,
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    "Active Orders",
                    style: ThemeText.statsDesc,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
