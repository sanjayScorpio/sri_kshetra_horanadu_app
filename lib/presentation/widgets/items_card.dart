import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class ItemsCard extends StatelessWidget {
  const ItemsCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListCard(
      image: NetworkImage(
          "https://images.pexels.com/photos/164829/pexels-photo-164829.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"),
      title: "New Microphone Set",
      tags: ["#958456", "Goods"],
      childrenWidgets: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
            text: TextSpan(
              text: 'Rate : ',
              style: ThemeText.generalDescriptionLight,
              children: <TextSpan>[
                TextSpan(
                  text: '₹ 550 / Kg',
                  style: ThemeText.generalTextBold,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            "If I do not go from place to place and offer the message that the world is unreal, people will not hear",
            style: ThemeText.generalDescriptionLight,
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
          ),
        ],
      ),
    );
  }
}
