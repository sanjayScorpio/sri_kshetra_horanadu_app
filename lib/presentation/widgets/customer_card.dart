import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class CustomerCard extends StatelessWidget {
  const CustomerCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListCard(
      image: NetworkImage(
          "https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80"),
      title: "New Microphone Set",
      tags: ["#958456", "firm"],
      childrenWidgets: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "18AABCU9603R1ZM".toUpperCase(),
            style: ThemeText.gstNoDark,
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            "Mysuru - 570007".toUpperCase(),
            style: ThemeText.gstNoDark,
          ),
        ],
      ),
    );
  }
}
