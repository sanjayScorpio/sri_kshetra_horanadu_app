import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class GradientSearchBar extends StatefulWidget {
  final String labelText;
  final String hint;
  final Widget prefix;
  final Widget suffix;
  final TextInputType keyboardType;
  final TextEditingController controller;
  final int maxlines;
  final IconData prefixIcon;
  final Widget suffixIcon;
  final String Function(String) validator;
  final List<TextInputFormatter> inputFormatters;
  final TextInputAction textInputAction;
  final TextStyle style;
  final TextAlign textAlign;
  final FocusNode focusNode;
  final bool showText;
  final bool isTransparent;
  const GradientSearchBar({
    Key key,
    this.labelText,
    this.hint,
    this.prefix,
    this.suffix,
    this.keyboardType,
    this.controller,
    this.maxlines: 1,
    this.prefixIcon,
    this.suffixIcon,
    this.validator,
    this.inputFormatters,
    this.textInputAction,
    this.style,
    this.textAlign: TextAlign.left,
    this.showText: false,
    this.focusNode,
    this.isTransparent: false,
  }) : super(key: key);

  @override
  _GradientSearchBarState createState() => _GradientSearchBarState();
}

class _GradientSearchBarState extends State<GradientSearchBar> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
        gradient: widget.isTransparent
            ? LinearGradient(colors: <Color>[
                Colors.transparent,
                Colors.transparent,
              ])
            : ThemeGradients.linearGradientVertical,
      ),
      child: TextFormField(
        textAlign: widget.textAlign,
        textInputAction: widget.textInputAction,
        validator: widget.validator,
        controller: widget.controller,
        keyboardType: widget.keyboardType,
        focusNode: widget.focusNode,
        inputFormatters: widget.inputFormatters,
        cursorColor: ThemeColors.white,
        style: ThemeText.textInputWhite,
        onChanged: (String text) {
          widget.controller.addListener(() {
            setState(() {});
          });
        },
        maxLines: widget.maxlines,
        decoration: InputDecoration(
          prefixIcon: widget.prefixIcon != null
              ? Icon(
                  widget.prefixIcon,
                  color: ThemeColors.white,
                )
              : null,
          suffixIcon: widget.controller.text.isEmpty == true
              ? null
              : widget.suffixIcon != null
                  ? widget.suffixIcon
                  : null,
          prefix: widget.prefix,
          suffix: widget.suffix,
          prefixStyle: ThemeText.textInput,
          contentPadding: EdgeInsets.all(15.0),
          focusedBorder: widget.isTransparent
              ? OutlineInputBorder(
                  borderSide: BorderSide(
                    color: ThemeColors.white,
                    width: 2.0,
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(18.0),
                  ),
                )
              : InputBorder.none,
          enabledBorder: widget.isTransparent
              ? OutlineInputBorder(
                  borderSide: BorderSide(
                    color: ThemeColors.white,
                    width: 1.0,
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(18.0),
                  ),
                )
              : InputBorder.none,
          border: widget.isTransparent
              ? OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(18.0),
                  ),
                  borderSide: BorderSide(
                    color: ThemeColors.white,
                    width: 2.0,
                  ),
                )
              : OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(18.0),
                  ),
                ),
          //  filled: true,
          hintStyle: TextStyle(
            color: ThemeColors.googleButtonBg,
          ),
          hintText: widget.hint,
          //  fillColor: Colors.white70,
        ),
      ),
    );
  }
}
