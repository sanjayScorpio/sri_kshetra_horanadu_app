import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class OrderItem extends StatefulWidget {
  OrderItem({Key key}) : super(key: key);

  @override
  _OrderItemState createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * .15,
                height: MediaQuery.of(context).size.width * .15,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      'https://image.freepik.com/free-photo/healthy-vegan-food-veggie-cooking-concept-wooden-cutting-kitchen-board-with-fresh-green-vegetables-herbs-cereal-top-view_118925-2209.jpg',
                    ),
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.0),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 10,
                            ),
                            child: Text(
                              StringUtils.capitalize('Vegetable bucket'),
                              style: ThemeText.firmName,
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          GradientConatiner(
                            child: Text(
                              StringUtils.capitalize("#45354"),
                              style: ThemeText.signOutWhite,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          GradientConatiner(
                            child: Text(
                              StringUtils.capitalize("firm"),
                              style: ThemeText.signOutWhite,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          GradientConatiner(
                            child: Text(
                              StringUtils.capitalize("GST - (18%)"),
                              style: ThemeText.signOutWhite,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "Qty (Kg)",
                      style: ThemeText.itemTitleText,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "6",
                      style: ThemeText.itemValueText,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Rate (₹ / Kg)",
                      style: ThemeText.itemTitleText,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "300",
                      style: ThemeText.itemValueText,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "Amount (₹)",
                      style: ThemeText.itemTitleText,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "1800",
                      style: ThemeText.itemValueText,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
