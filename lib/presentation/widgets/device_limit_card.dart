import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class DeviceLimitCard extends StatefulWidget {
  DeviceLimitCard({Key key}) : super(key: key);

  @override
  _DeviceLimitCardState createState() => _DeviceLimitCardState();
}

class _DeviceLimitCardState extends State<DeviceLimitCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            color: ThemeColors.white,
            borderRadius: BorderRadius.circular(30),
            boxShadow: [
              BoxShadow(
                color: ThemeColors.shadowGrey,
                offset: Offset(2, 2),
                blurRadius: 30.0,
              ),
            ],
          ),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Xiaomi MI A2",
                      style: ThemeText.deviceName,
                    ),
                    Text(
                      "Sign Out",
                      style: ThemeText.signOutSecondary,
                    ),
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                RichText(
                  text: TextSpan(
                    text: 'Device Id : ',
                    style: ThemeText.deviceIdLight,
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Register',
                        style: ThemeText.deviceIdDark,
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                RichText(
                  text: TextSpan(
                    text: 'Last Session : ',
                    style: ThemeText.deviceIdLight,
                    children: <TextSpan>[
                      TextSpan(
                        text: '12th Feb 2020',
                        style: ThemeText.deviceIdDark,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
