import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

Future<dynamic> bottomPageModal({
  @required BuildContext context,
  VoidCallback onPress,
  String sampleText,
  String titleText,
  bool isBackEnabled: false,
  bool isCancelEnabled: false,
  bool isTitleRequired: true,
  double height,
  @required Widget child,
}) async {
  return showModalBottomSheet(
    context: context,
    enableDrag: true,
    isScrollControlled: true,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(15),
        topRight: Radius.circular(15),
      ),
    ),
    builder: (BuildContext context) {
      return Container(
        height: height,
        constraints: BoxConstraints(
          maxHeight: MediaQuery.of(context).size.height - 80,
        ),
        decoration: BoxDecoration(
          color: ThemeColors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15),
          ),
        ),
        child: Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Wrap(
            children: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      if (isBackEnabled || isCancelEnabled || isTitleRequired)
                        Stack(
                          alignment: Alignment.center,
                          children: [
                            if (isBackEnabled) GoBackButton(),
                            if (isTitleRequired) ...{
                              Center(
                                child: Text(
                                  titleText,
                                  style: ThemeText.pageHeaderTitleSecondaryDark,
                                ),
                              ),
                            },
                            if (isCancelEnabled)
                              GestureDetector(
                                onTap: () => {Navigator.of(context).pop()},
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "cancel",
                                    style: ThemeText.cancelText,
                                  ),
                                ),
                              )
                          ],
                        ),
                      if (isBackEnabled || isCancelEnabled || isTitleRequired)
                        SizedBox(
                          height: 15,
                        ),
                      child
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
// padding: const EdgeInsets.symmetric(
//                             vertical: 5.0, horizontal: 15.0),
