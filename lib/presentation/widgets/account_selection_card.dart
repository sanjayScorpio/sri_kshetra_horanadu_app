import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/gradient_container.dart';

class AccountSelectionCard extends StatefulWidget {
  AccountSelectionCard({Key key}) : super(key: key);

  @override
  _AccountSelectionCardState createState() => _AccountSelectionCardState();
}

class _AccountSelectionCardState extends State<AccountSelectionCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(12.0),
      decoration: BoxDecoration(
        color: ThemeColors.white,
        borderRadius: BorderRadius.circular(30),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * .20,
            height: MediaQuery.of(context).size.width * .20,
            decoration: BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(
                      'https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80')),
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    StringUtils.capitalize("Cisco Networks main stream"),
                    style: ThemeText.firmName,
                    overflow: TextOverflow.fade,
                    softWrap: false,
                    maxLines: 1,
                  ),
                  Text(
                    "18AABCU9603R1ZM".toUpperCase(),
                    style: ThemeText.gstNoDark,
                  ),
                  Row(
                    children: [
                      GradientConatiner(
                        child: Text(
                          StringUtils.capitalize("Essentials"),
                          style: ThemeText.generalButtonText,
                        ),
                        gradientType: "primary",
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      GradientConatiner(
                        gradientType: "primary",
                        child: Text(
                          StringUtils.capitalize("Owner"),
                          style: ThemeText.generalButtonText,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Icon(
              MdiIcons.chevronRight,
              color: ThemeColors.secondaryDark,
              size: 50,
            ),
          ),
        ],
      ),
    );
  }
}
