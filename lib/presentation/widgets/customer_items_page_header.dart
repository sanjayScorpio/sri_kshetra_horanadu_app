import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/colors.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class ListAppHeader extends StatelessWidget {
  final String listName;
  final int listCount;

  ListAppHeader({this.listName, this.listCount});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeColors.white,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  "Slips",
                  style: ThemeText.searchHeaderTitle,
                ),
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: GestureDetector(
                        onTap: () => {},
                        child: Center(
                            child: Icon(
                          MdiIcons.bellOutline,
                          color: ThemeColors.darkBlue,
                          size: 30,
                        )),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 5.0),
                      child: GestureDetector(
                        onTap: () => {},
                        child: Center(
                            child: Icon(
                          MdiIcons.accountCircleOutline,
                          color: ThemeColors.darkBlue,
                          size: 30,
                        )),
                      ),
                    )
                  ],
                ))
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Total ${this.listName}",
                  style: ThemeText.generalDescriptionLight,
                ),
                Text(
                  this.listCount.toString(),
                  style: ThemeText.generalDescriptionBold,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
