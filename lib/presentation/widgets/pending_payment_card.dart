import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class PendingPaymentCard extends StatelessWidget {
  final ImageProvider image;
  final String title;
  final String tag;
  final bool isInvoice;
  final String amount;
  const PendingPaymentCard({
    Key key,
    this.image,
    this.title,
    this.tag,
    this.isInvoice: false,
    this.amount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: ThemeColors.white,
        boxShadow: [
          BoxShadow(
            color: ThemeColors.shadowGrey,
            offset: Offset(2, 10),
            blurRadius: 30.0,
          ),
        ],
      ),
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * .12,
                height: MediaQuery.of(context).size.width * .12,
                decoration: BoxDecoration(
                  image: DecorationImage(fit: BoxFit.cover, image: image),
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.0),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        StringUtils.capitalize(this.title),
                        style: ThemeText.firmName,
                        // overflow: TextOverflow.fade,
                        // softWrap: false,
                        maxLines: 2,
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      GradientConatiner(
                        child: Text(
                          StringUtils.capitalize(this.tag),
                          style: ThemeText.notificationDescription,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  this.amount,
                  style: this.isInvoice == true
                      ? ThemeText.dashboardTileValueErrorRedMini
                      : ThemeText.dashboardTileValueSuccessGreenMini,
                ),
              )
            ],
          )),
    );
  }
}
