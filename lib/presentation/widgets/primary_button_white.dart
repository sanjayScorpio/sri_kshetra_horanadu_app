import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class PrimaryButtonWhite extends StatelessWidget {
  final String title;
  final Function onPress;
  const PrimaryButtonWhite({Key key, @required this.title, @required this.onPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: onPress,
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.all(15),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: ThemeColors.white,
            borderRadius: BorderRadius.circular(18.0),
          ),
          child: Text(
            this.title,
            style: ThemeText.generalButtonTextPrimary,
          ),
        ),
      ),
    );
  }
}
