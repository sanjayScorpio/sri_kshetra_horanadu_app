import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class GradientConatiner extends StatelessWidget {
  final Widget child;
  final String gradientType;
  const GradientConatiner({Key key, this.child, this.gradientType})
      : super(key: key);

  _primary() {
    return BoxDecoration(
        gradient: ThemeGradients.linearGradientVertical,
        borderRadius: BorderRadius.circular(10));
  }

  _white() {
    return BoxDecoration(
      color: ThemeColors.white,
      borderRadius: BorderRadius.circular(10),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: gradientType == "white" ? _white() : _primary(),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 10.0),
        child: child,
      ),
    );
  }
}
