import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

Future<dynamic> dialogModal({
  @required BuildContext context,
  @required String title,
  VoidCallback onPress,
}) async {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: Wrap(
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Radio(
                      value: true,
                      groupValue: null,
                      onChanged: null,
                      toggleable: true,
                    ),
                    Text(
                      "Delivered",
                      style: ThemeText.filterText,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Radio(
                      value: false,
                      groupValue: null,
                      onChanged: null,
                      toggleable: true,
                    ),
                    Text(
                      "Pending",
                      style: ThemeText.filterText,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Radio(
                      value: false,
                      groupValue: null,
                      onChanged: null,
                      toggleable: true,
                    ),
                    Text(
                      "Canceled",
                      style: ThemeText.filterText,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
        actions: <Widget>[
          TextButton(
            child: Text(
              'Cancel',
              style: ThemeText.cardDescRed,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text(
              'Update',
              style: ThemeText.cardDescSecondary,
            ),
            onPressed: () {},
          ),
        ],
      );
    },
  );
}
