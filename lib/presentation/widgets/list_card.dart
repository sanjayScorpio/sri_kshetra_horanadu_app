import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class ListCard extends StatelessWidget {
  final ImageProvider image;
  final String title;
  final List<String> tags;
  final Widget childrenWidgets;
  ListCard({this.image, this.title, this.tags, this.childrenWidgets});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(12.0),
      decoration: BoxDecoration(
        color: ThemeColors.white,
        borderRadius: BorderRadius.circular(25),
        boxShadow: [
          BoxShadow(
            color: ThemeColors.shadowGrey,
            offset: Offset(2, 2),
            blurRadius: 30.0,
          ),
        ],
      ),
      child: Stack(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * .22,
                height: MediaQuery.of(context).size.width * .22,
                decoration: BoxDecoration(
                  image: DecorationImage(fit: BoxFit.cover, image: image),
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.0),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        StringUtils.capitalize(this.title),
                        style: ThemeText.firmName,
                        // overflow: TextOverflow.fade,
                        // softWrap: false,
                        maxLines: 2,
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        children: [
                          for (var tag in tags) ...[
                            GradientConatiner(
                              child: Text(
                                StringUtils.capitalize(tag),
                                style: ThemeText.generalButtonText,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                          ]
                        ],
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      this.childrenWidgets
                    ],
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            top: 5,
            right: 0,
            child: Icon(MdiIcons.dotsVertical),
          )
        ],
      ),
    );
  }
}
