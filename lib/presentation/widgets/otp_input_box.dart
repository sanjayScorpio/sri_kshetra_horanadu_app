import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class OTPTextBox extends StatefulWidget {
  OTPTextBox({Key key}) : super(key: key);

  @override
  _OTPTextBoxState createState() => _OTPTextBoxState();
}

class _OTPTextBoxState extends State<OTPTextBox> {
  @override
  Widget build(BuildContext context) {
    return Form(
      child: PinCodeTextField(
        appContext: context,
        enablePinAutofill: true,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        pastedTextStyle: TextStyle(
          color: ThemeColors.secondary,
          fontWeight: FontWeight.w600,
          backgroundColor: ThemeColors.secondary,
        ),
        length: 4,
        obscureText: false,
        obscuringCharacter: '*',
        animationType: AnimationType.fade,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp('[0-9]'))
        ],
        validator: (v) {
          if (v.isEmpty) {
            return "please enter the valid OTP number";
          } else {
            return null;
          }
        },
        pinTheme: PinTheme(
          shape: PinCodeFieldShape.box,
          borderRadius: BorderRadius.circular(10),
          fieldHeight: 50,
          fieldWidth: 50,
          activeFillColor: Colors.white,
          activeColor: ThemeColors.secondary,
          inactiveColor: ThemeColors.lightGrey,
          inactiveFillColor: ThemeColors.white,
          selectedFillColor: ThemeColors.white,
          selectedColor: ThemeColors.secondaryDark,
        ),
        cursorColor: ThemeColors.darkBlue,
        animationDuration: Duration(milliseconds: 300),
        textStyle: TextStyle(
          fontSize: 20,
          height: 1.6,
        ),
        backgroundColor: ThemeColors.white,
        enableActiveFill: true,
        // controller: textEditingController,
        keyboardType: TextInputType.number,
        boxShadows: [
          BoxShadow(
            offset: Offset(0, 1),
            color: Colors.black12,
            blurRadius: 10,
          )
        ],
        onCompleted: (v) {
          print("Completed");
        },
        // onTap: () {
        //   print("Pressed");
        // },
        onChanged: (value) {
          print(value);
          // setState(() {
          //   currentText = value;
          // });
        },
        beforeTextPaste: (text) {
          print("Allowing to paste $text");
          //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
          //but you can show anything you want here, like your pop up saying wrong paste format or etc
          return true;
        },
      ),
    );
  }
}
