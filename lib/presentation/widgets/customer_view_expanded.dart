import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class CustomerExpandedView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height -275,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            CustomerDetailsCard(
              icon: MdiIcons.attachment,
              iconBG: ThemeColors.violet,
              title: "GSTIN",
              value: "855UF2W34W",
            ),
            SizedBox(
              height: 20,
            ),
            CustomerDetailsCard(
              icon: MdiIcons.accountOutline,
              iconBG: ThemeColors.infoOrange,
              title: "Contact Person",
              value: "Sddirath Malhotra",
            ),
            SizedBox(
              height: 20,
            ),
            CustomerDetailsCard(
              icon: MdiIcons.cellphoneAndroid,
              iconBG: ThemeColors.errorRed,
              title: "Mobile",
              value: "+919481410198",
            ),
            SizedBox(
              height: 20,
            ),
            CustomerDetailsCard(
              icon: MdiIcons.emailOutline,
              iconBG: ThemeColors.successGreen,
              title: "E-mail",
              value: "someone@yourmail.com",
            ),
            SizedBox(
              height: 20,
            ),
            CustomerDetailsCard(
              icon: MdiIcons.mapOutline,
              iconBG: ThemeColors.skyBlue,
              title: "Address",
              value:
                  "# 4423/1 8th cross, Narayana shastri road, Vidyaranyapuram, Bengaluru North",
            ),
            SizedBox(
              height: 20,
            ),
            CustomerDetailsCard(
              icon: MdiIcons.mapMarkerRadiusOutline,
              iconBG: ThemeColors.royalBlue,
              title: "Place",
              value: "Bengaluru - 570001",
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
