import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class CustomerQuickActionLinks extends StatelessWidget {
  Widget iconList(IconData iconData, Color color) {
    return Column(
      children: [
        Container(
          width: 45.0,
          height: 45.0,
          decoration: BoxDecoration(
            color: ThemeColors.white,
            borderRadius: BorderRadius.all(Radius.circular(15)),
          ),
          child: Center(
            child: Icon(
              iconData,
              color: color,
              size: 25,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          iconList(MdiIcons.phone, ThemeColors.safforn),
          iconList(MdiIcons.messageTextOutline, ThemeColors.errorRed),
          iconList(MdiIcons.whatsapp, ThemeColors.green),
          iconList(MdiIcons.emailOutline, ThemeColors.skyBlue),
        ],
      ),
    );
  }
}
