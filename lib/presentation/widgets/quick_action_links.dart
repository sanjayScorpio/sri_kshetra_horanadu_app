import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/colors.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/gradients.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class QuickActionLinks extends StatelessWidget {
  const QuickActionLinks({Key key}) : super(key: key);

  Widget iconList(String text, IconData iconData) {
    return Column(
      children: [
        Container(
          width: 50.0,
          height: 50.0,
          decoration: BoxDecoration(
            gradient: ThemeGradients.linearGradientVertical,
            borderRadius: BorderRadius.all(Radius.circular(18)),
          ),
          child: Center(
            child: Icon(
              iconData,
              color: ThemeColors.white,
              size: 30,
            ),
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Text(
          text,
          style: ThemeText.quickLinkText,
          textAlign: TextAlign.center,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          iconList("Create Slip", MdiIcons.textBoxPlusOutline),
          iconList("Create Order", MdiIcons.fileDocumentEditOutline),
          iconList("Add Invoice", MdiIcons.fileDocumentEditOutline),
          iconList("Receive \nPayment", MdiIcons.currencyInr),
        ],
      ),
    );
  }
}
