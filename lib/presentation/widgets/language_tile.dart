import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class LanguageTile extends StatelessWidget {
  final bool isActive;
  final Function onPress;
  final Map<String, String> item;

  LanguageTile(this.item, this.isActive, this.onPress);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        decoration: BoxDecoration(
          color: isActive ? ThemeColors.primary : ThemeColors.white,
          border: isActive
              ? Border.all(color: ThemeColors.primary)
              : Border.all(color: ThemeColors.white),
          borderRadius: BorderRadius.circular(45.0),
          boxShadow: [
            BoxShadow(
              color: ThemeColors.shadowGrey,
              offset: Offset(2, 2),
              blurRadius: 30.0,
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              item['title'],
              style: isActive
                  ? ThemeText.headerLargeWhite
                  : ThemeText.headerLargeDark,
            ),
            Text(
              item['lang'],
              style: isActive
                  ? ThemeText.languageTextWhite
                  : ThemeText.languageText,
            )
          ],
        ),
      ),
    );
  }
}
