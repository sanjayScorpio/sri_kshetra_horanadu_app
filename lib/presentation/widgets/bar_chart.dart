import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/colors.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/bar_chart_sample.dart';

class BarChart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: ThemeColors.white,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Color(0xFFd6d6d6),
            offset: Offset(2, 2),
          ),
        ],
        border: Border.all(color: ThemeColors.secondary, width: 0.5),
      ),
      child: Center(
        child: BarChartSample2(),
      ),
    );
  }
}
