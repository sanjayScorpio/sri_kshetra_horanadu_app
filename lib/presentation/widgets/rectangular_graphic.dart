import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class RectangularGraphic extends StatelessWidget {
  const RectangularGraphic({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: ThemeColors.primary,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30.0),
          bottomRight: Radius.circular(30.0),
        ),
      ),
      child: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            bottom: 0,
            child: Image.asset("assets/images/dots.png"),
          ),
          Positioned(
            top: -100.0,
            right: -100.0,
            child: Image.asset("assets/images/rect_right.png"),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 25.0, horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                    text: 'Welcome to ',
                    style: ThemeText.welcomeHeader,
                    children: <TextSpan>[
                      TextSpan(
                        text: 'SLIPS',
                        style: ThemeText.welcomeHeaderBold,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  "Bring together your orders, payments, tasks, people.",
                  style: ThemeText.welcomeHeaderDescription,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
