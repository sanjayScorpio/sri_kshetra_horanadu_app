import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class FullPageRectangularGraphic extends StatelessWidget {
  final Widget children;
  FullPageRectangularGraphic(this.children);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeColors.secondary,
      child: Stack(
        children: [
          Positioned(
            top: 150.0,
            left: 50.0,
            child: Image.asset("assets/images/dots.png"),
          ),
          Positioned(
            top: -100.0,
            right: -100.0,
            child: Image.asset("assets/images/rect_right.png"),
          ),
          Positioned(
            top: 0.0,
            right: 0.0,
            bottom: 0.0,
            left: -450.0,
            child: Image.asset("assets/images/rect_left.png"),
          ),
          // this.children
          Container(height: double.infinity, child: this.children),
        ],
      ),
    );
  }
}
