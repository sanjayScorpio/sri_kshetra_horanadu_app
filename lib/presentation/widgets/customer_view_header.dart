import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/customer_quick_linkss.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/primary_header_container.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class CustomerViewHeader extends StatelessWidget {
  final Function onPressExpand;
  final bool isExpand;
  CustomerViewHeader({
    this.isExpand,
    this.onPressExpand,
  });

  @override
  Widget build(BuildContext context) {
    return PrimaryHeaderContainer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () => {AppRouter.goBack()},
                  child: Icon(
                    MdiIcons.chevronLeft,
                    color: ThemeColors.white,
                    size: 30,
                  ),
                ),
                Text(
                  "Customer",
                  style: ThemeText.pageHeaderTitleWhite,
                ),
                Icon(
                  MdiIcons.dotsVertical,
                  color: ThemeColors.white,
                  size: 30,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * .15,
                  height: MediaQuery.of(context).size.width * .15,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                            'https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80')),
                    borderRadius: BorderRadius.all(Radius.circular(15.0)),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          StringUtils.capitalize(
                            "Cisco Networks main stream Cisco Networks main stream",
                          ),
                          style: ThemeText.customerNameMedium,
                          overflow: TextOverflow.ellipsis,
                          softWrap: false,
                          maxLines: 2,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            GradientConatiner(
                              gradientType: "white",
                              child: Text(
                                StringUtils.capitalize("Essentials"),
                                style: ThemeText.generalButtonTextPrimarySmall,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            GradientConatiner(
                              gradientType: "white",
                              child: Text(
                                StringUtils.capitalize("Owner"),
                                style: ThemeText.generalButtonTextPrimarySmall,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            child: CustomerQuickActionLinks(),
          ),
         if (isExpand) ...[
            CustomerExpandedView(),
         ],
          if (!isExpand) ...{
            Center(
              child: IconButton(
                onPressed: onPressExpand,
                icon: Icon(
                  MdiIcons.chevronDown,
                  color: ThemeColors.white,
                  size: 30,
                ),
              ),
            )
          },
        ],
      ),
    );
  }
}
