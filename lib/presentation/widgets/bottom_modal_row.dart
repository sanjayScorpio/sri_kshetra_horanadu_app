import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class BottomModalRow extends StatefulWidget {
  final Widget child;
  final String text;
  BottomModalRow({
    Key key,
    this.child,
    this.text,
  }) : super(key: key);

  @override
  _BottomModalRowState createState() => _BottomModalRowState();
}

class _BottomModalRowState extends State<BottomModalRow> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          widget.text,
          style: ThemeText.bottomSheetText,
        ),
        widget.child
      ],
    );
  }
}
