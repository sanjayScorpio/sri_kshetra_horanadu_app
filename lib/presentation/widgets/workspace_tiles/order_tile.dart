import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class OrderTile extends StatefulWidget {
  OrderTile({Key key}) : super(key: key);

  @override
  _OrderTileState createState() => _OrderTileState();
}

class _OrderTileState extends State<OrderTile> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.all(12.0),
          decoration: BoxDecoration(
            color: ThemeColors.white,
            borderRadius: BorderRadius.circular(25),
            border: Border.all(color: ThemeColors.primary, width: 0.5),
            boxShadow: [
              BoxShadow(
                color: ThemeColors.shadowGrey,
                offset: Offset(2, 2),
                blurRadius: 30.0,
              ),
            ],
          ),
          child: Column(
            children: [
              SizedBox(
                width: 10,
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: Text(
                  "Order",
                  style: ThemeText.cardDescSecondary,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        dataItem(
                          title: "Date",
                          value: '12/12/2020',
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        dataItem(
                          title: "No of items",
                          value: '16',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        dataItem(
                          title: "Order No",
                          value: '584584',
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        dataItem(
                          title: "Deliver on",
                          value: '12/1/2020 5:30 PM',
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: ThemeColors.darkGrey.withOpacity(0.1),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Total amount",
                        style: ThemeText.orderTotalAmount,
                      ),
                      Text(
                        "₹5000",
                        style: ThemeText.primaryBold,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
        Positioned(
          bottom: 5,
          right: 15,
          child: Text(
            "12/12/2020, 2 : 30 PM",
            style: ThemeText.messageInfo,
          ),
        ),
        Positioned(
          top: 10,
          right: 10,
          child: GestureDetector(
            onTap: () {},
            child: Icon(MdiIcons.dotsVertical),
          ),
        ),
      ],
    );
  }

  Widget dataItem({title, value}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: ThemeText.generalDescriptionLight,
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          value,
          style: ThemeText.orderTextDark,
        ),
      ],
    );
  }
}
