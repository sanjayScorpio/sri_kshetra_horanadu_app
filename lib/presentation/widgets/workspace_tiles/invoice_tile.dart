import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class InvoiceTile extends StatefulWidget {
  InvoiceTile({Key key}) : super(key: key);

  @override
  _InvoiceTileState createState() => _InvoiceTileState();
}

class _InvoiceTileState extends State<InvoiceTile> {
  final List items = [
    'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  ];
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            color: ThemeColors.white,
            borderRadius: BorderRadius.circular(25),
            border: Border.all(color: ThemeColors.primary, width: 0.5),
            boxShadow: [
              BoxShadow(
                color: ThemeColors.shadowGrey,
                offset: Offset(2, 2),
                blurRadius: 30.0,
              ),
            ],
          ),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: Column(
            children: [
              SizedBox(
                width: 10,
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: Text(
                  "Invoice",
                  style: ThemeText.cardDescSecondary,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        dataItem(
                          title: "Invoice Date",
                          value: '12/12/2020',
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        dataItem(
                          title: "Invoice No",
                          value: '584584',
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    if (items.length > 4) ...{
                      for (var i = 0; i < 4; i++) ...[
                        GestureDetector(
                          onTap: () => {},
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 10,
                              vertical: 10,
                            ),
                            width: 55,
                            height: 55,
                            decoration: BoxDecoration(
                              color: ThemeColors.secondary,
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(items[i]),
                              ),
                            ),
                          ),
                        ),
                      ],
                      GestureDetector(
                        onTap: () => {},
                        child: Stack(
                          children: [
                            Container(
                              width: 55,
                              height: 55,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                image: DecorationImage(
                                  image: NetworkImage(
                                    'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                                  ),
                                  fit: BoxFit.cover,
                                ),
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.5),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Center(
                                  child: Text(
                                    "+2",
                                    style: ThemeText.welcomeHeader,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    } else ...{
                      for (var item in items) ...[
                        GestureDetector(
                          onTap: () => {},
                          child: Container(
                            width: 55,
                            height: 55,
                            decoration: BoxDecoration(
                              color: ThemeColors.secondary,
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(item),
                              ),
                            ),
                          ),
                        ),
                      ],
                    },
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                child: fileItem(),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                child: fileItem(),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: ThemeColors.darkGrey.withOpacity(0.1),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Total amount",
                        style: ThemeText.orderTotalAmount,
                      ),
                      Text(
                        "₹5000",
                        style: ThemeText.primaryBold,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
        Positioned(
          bottom: 5,
          right: 15,
          child: Text(
            "12/12/2020, 2 : 30 PM",
            style: ThemeText.messageInfo,
          ),
        ),
        Positioned(
          top: 10,
          right: 10,
          child: GestureDetector(
            onTap: () {},
            child: Icon(MdiIcons.dotsVertical),
          ),
        ),
      ],
    );
  }

  Widget dataItem({title, value}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: ThemeText.generalDescriptionLight,
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          value,
          style: ThemeText.orderTextDark,
        ),
      ],
    );
  }

  Widget fileItem({title, value}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
            color: ThemeColors.secondary,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Center(
            child: Icon(
              MdiIcons.filePdfOutline,
              color: ThemeColors.white,
              size: 40,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Text(
                        "Karnataka_state_startup_policy and_Rules.pdf",
                        style: ThemeText.orderTextDark,
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Icon(
                      MdiIcons.downloadOutline,
                      size: 30,
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    Text(
                      '16.5 MB',
                      style: ThemeText.generalDescriptionLight,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      '25 Pages',
                      style: ThemeText.generalDescriptionLight,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Container topRowContainer(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * .15,
                height: MediaQuery.of(context).size.width * .15,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      'https://mk0qomafemocnned3wjh.kinstacdn.com/wp-content/uploads/2019/02/AI-thispersondoesnotexist.jpg',
                    ),
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.0),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            StringUtils.capitalize('Sudeep Sharma'),
                            style: ThemeText.firmName,
                            overflow: TextOverflow.fade,
                            maxLines: 1,
                          ),
                          GestureDetector(
                            child: Icon(MdiIcons.dotsVertical),
                            onTap: () {},
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      GradientConatiner(
                        child: Text(
                          StringUtils.capitalize("#45354"),
                          style: ThemeText.signOutWhite,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
