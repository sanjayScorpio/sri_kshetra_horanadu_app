import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class PaymentTile extends StatefulWidget {
  PaymentTile({Key key}) : super(key: key);

  @override
  _PaymentTileState createState() => _PaymentTileState();
}

class _PaymentTileState extends State<PaymentTile> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            color: ThemeColors.white,
            borderRadius: BorderRadius.circular(25),
            border: Border.all(color: ThemeColors.primary, width: 0.5),
            boxShadow: [
              BoxShadow(
                color: ThemeColors.shadowGrey,
                offset: Offset(2, 2),
                blurRadius: 30.0,
              ),
            ],
          ),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: Column(
            children: [
              SizedBox(
                width: 10,
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: Text(
                  "Payment",
                  style: ThemeText.cardDescSecondary,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                alignment: Alignment.centerLeft,
                child: RichText(
                  text: TextSpan(
                    text: 'Received ',
                    style: ThemeText.orderTextDark,
                    children: <TextSpan>[
                      TextSpan(
                        text: '  ₹ 5,000  ',
                        style: ThemeText.dashboardTileValueSuccessGreenMini,
                      ),
                      TextSpan(
                        text: 'on ',
                      ),
                      TextSpan(
                        text: ' 12/12/2020  ',
                        style: ThemeText.generalDescriptionDarkBold,
                      ),
                      TextSpan(
                        text: 'via ',
                      ),
                      TextSpan(
                        text: ' Cash',
                        style: ThemeText.secondaryHeader,
                      ),
                      TextSpan(
                        text: '.',
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                  "Web design refers to the design of websites that are displayed on the internet. It usually refers to the user experience aspects of website development",
                  style: ThemeText.generalDescriptionLight,
                ),
              ),
            ],
          ),
        ),
        Positioned(
          bottom: 5,
          right: 15,
          child: Text(
            "12/12/2020, 2 : 30 PM",
            style: ThemeText.messageInfo,
          ),
        ),
        Positioned(
          top: 10,
          right: 10,
          child: GestureDetector(
            onTap: () {},
            child: Icon(MdiIcons.dotsVertical),
          ),
        ),
      ],
    );
  }
}
