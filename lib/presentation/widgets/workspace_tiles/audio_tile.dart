import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seekbar/seekbar.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class AudioTile extends StatefulWidget {
  AudioTile({Key key}) : super(key: key);

  @override
  _AudioTileState createState() => _AudioTileState();
}

class _AudioTileState extends State<AudioTile> {
  double _value = 0;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            color: ThemeColors.white,
            borderRadius: BorderRadius.circular(25),
            border: Border.all(color: ThemeColors.primary, width: 0.5),
            boxShadow: [
              BoxShadow(
                color: ThemeColors.shadowGrey,
                offset: Offset(2, 2),
                blurRadius: 30.0,
              ),
            ],
          ),
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: Column(
            children: [
              SizedBox(
                width: 10,
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: Text(
                  "Audio",
                  style: ThemeText.cardDescSecondary,
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Row(
                  children: [
                    Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        color: ThemeColors.secondary,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Center(
                        child: Icon(
                          MdiIcons.play,
                          color: ThemeColors.white,
                          size: 40,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Slider(
                              min: 0,
                              value: _value,
                              activeColor: ThemeColors.secondary,
                              inactiveColor: ThemeColors.lightGrey,
                              max: 100,
                              onChanged: (double value) {
                                setState(() {
                                  _value = value;
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Text(
                      "3:30",
                      style: ThemeText.generalButtonTextGrey,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Positioned(
          bottom: 5,
          right: 15,
          child: Text(
            "12/12/2020, 2 : 30 PM",
            style: ThemeText.messageInfo,
          ),
        ),
        Positioned(
          top: 10,
          right: 10,
          child: GestureDetector(
            onTap: () {},
            child: Icon(MdiIcons.dotsVertical),
          ),
        ),
      ],
    );
  }
}
