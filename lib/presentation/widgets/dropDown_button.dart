import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class CustomDropDownButton extends StatefulWidget {
  final List<String> items;
  final String defaultText;
  CustomDropDownButton({Key key, this.items, this.defaultText})
      : super(key: key);

  @override
  _CustomDropDownButtonState createState() => _CustomDropDownButtonState();
}

class _CustomDropDownButtonState extends State<CustomDropDownButton> {
  String _dropdownValue;

  @override
  Widget build(BuildContext context) {
    final dropDownMenuOptions =
        widget.items.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(
          value,
          style: ThemeText.generalButtonText,
        ),
      );
    }).toList();
    return Container(
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
          value: _dropdownValue,
          hint: Text(
            widget.defaultText,
            style: ThemeText.generalButtonText,
          ),
          icon: Icon(
            MdiIcons.menuDown,
            color: ThemeColors.white,
          ),
          iconSize: 25,
          underline: SizedBox(),
          onChanged: (String string) => setState(() => _dropdownValue = string),
          items: dropDownMenuOptions,
        ),
      ),
    );
  }
}
