import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class DatePickerButton extends StatefulWidget {
  final double textBoxWidth;
  final bool isTimeRequired;
  DatePickerButton({
    Key key,
    this.textBoxWidth,
    this.isTimeRequired: false,
  }) : super(key: key);

  @override
  _DatePickerButtonState createState() => _DatePickerButtonState();
}

class _DatePickerButtonState extends State<DatePickerButton> {
  DateTime _date = DateTime.now();
  TimeOfDay time = TimeOfDay.now();

  Future<Null> _selectedDate(BuildContext context) async {
    final DateTime datePicker = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime.now(),
      errorFormatText: 'Enter valid date',
      fieldLabelText: 'Order date',
      errorInvalidText: 'Enter date in valid range',
    );

    if (datePicker != null && datePicker != _date) {
      print("Date selected ${_date.toString()}");
      setState(() {
        _date = datePicker;
      });
      if (widget.isTimeRequired) {
        _pickTime(context);
      }
    } else {
      _date = DateTime.now();
    }
  }

  Future<TimeOfDay> _pickTime(BuildContext context) async {
    TimeOfDay pickTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (pickTime != null)
      setState(() {
        time = pickTime;
      });
  }

  @override
  Widget build(BuildContext context) {
    // double textBoxWidth =
    //     (MediaQuery.of(context).size.width / 2).toDouble() - 50;
    return GestureDetector(
      onTap: () => _selectedDate(context),
      child: Container(
        width: widget.textBoxWidth,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(18),
          color: ThemeColors.primary,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.isTimeRequired == true
                    ? (DateFormat('dd-MM-yyyy \n').format(_date) +
                        (time.hour.toString() + ' : ' + time.minute.toString()))
                    : DateFormat('dd-MM-yyyy').format(_date),
                style: ThemeText.generalButtonText,
                textAlign: TextAlign.center,
              ),
              SizedBox(
                width: 10,
              ),
              Icon(
                MdiIcons.calendarOutline,
                color: ThemeColors.white,
                size: 18,
              )
            ],
          ),
        ),
      ),
    );
  }
}
