import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class GoogleAuthButton extends StatelessWidget {
  final String title;
  GoogleAuthButton(this.title);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Tap'),
        ));
      },
      child: Container(
        decoration: BoxDecoration(
          color: ThemeColors.googleButtonBg,
          borderRadius: BorderRadius.circular(18),
          border: Border.all(color: ThemeColors.shadowGrey),
          boxShadow: [
            BoxShadow(
              color: ThemeColors.shadowGrey,
              offset: Offset(2, 10),
              blurRadius: 30.0,
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: 20.0,
                  height: 20.0,
                  child: Image.asset("assets/images/googlelogo.png"),
                ),
              ),
              SizedBox(width: 20.0),
              Text(
                this.title,
                style: ThemeText.googleText,
              )
            ],
          ),
        ),
      ),
    );
  }
}
