import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/add_invoice.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class CustomerPaymentTextBox extends StatefulWidget {
  CustomerPaymentTextBox({Key key}) : super(key: key);

  @override
  _CustomerPaymentTextBoxState createState() => _CustomerPaymentTextBoxState();
}

class _CustomerPaymentTextBoxState extends State<CustomerPaymentTextBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeColors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: ThemeColors.darkGrey.withOpacity(0.15),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Closing balance",
                      style: ThemeText.secondaryTextDark,
                    ),
                    Text(
                      "₹5000",
                      style: ThemeText.primaryBold,
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          GestureDetector(
            onTap: () => addInvoiceModal(context: context),
            child: Container(
              width: 45,
              height: 45,
              child: Icon(
                MdiIcons.plus,
                size: 20,
                color: ThemeColors.white,
              ),
              decoration: BoxDecoration(
                color: ThemeColors.secondary,
                shape: BoxShape.circle,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
