import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/custom_switch.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/gradient_container.dart';

class ItemCustomerSection extends StatefulWidget {
  ItemCustomerSection({Key key}) : super(key: key);

  @override
  _ItemCustomerSectionState createState() => _ItemCustomerSectionState();
}

class _ItemCustomerSectionState extends State<ItemCustomerSection> {
  bool status = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(12.0),
      decoration: BoxDecoration(),
      child: Stack(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * .18,
                height: MediaQuery.of(context).size.width * .18,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                        "https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80"),
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.0),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Flexible(
                            child: Text(
                              StringUtils.capitalize('Sudeep Sharma & Co'),
                              style: ThemeText.firmName,
                              overflow: TextOverflow.fade,
                              // softWrap: false,
                              maxLines: 1,
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: ThemeColors.successGreen.withOpacity(0.2),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0, vertical: 5),
                              child: Text(
                                'Delivered',
                                style: ThemeText.orderStatusGreen,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        children: [
                          GradientConatiner(
                            child: Text(
                              StringUtils.capitalize("#958456"),
                              style: ThemeText.signOutWhite,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          GradientConatiner(
                            child: Text(
                              StringUtils.capitalize("firm"),
                              style: ThemeText.signOutWhite,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      RichText(
                        text: TextSpan(
                          text: '18AABCU9603R1ZM ',
                          style: ThemeText.generalDescriptionLight,
                          children: <TextSpan>[
                            TextSpan(
                              text: ' +919585858585',
                              style: ThemeText.generalDescriptionLight,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        "#4534, 6th cross, Vasanth kumar road, Vidyaranyapura, Bengaluru, Karnataka - 570007",
                        style: ThemeText.generalDescriptionLight,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          // Positioned(
          //   right: 5,
          //   top: 30,
          //   child: CustomSwitch(
          //     activeColor: ThemeColors.secondary,
          //     value: status,
          //     onChanged: (value) {
          //       print("VALUE : $value");
          //       setState(() {
          //         status = value;
          //       });
          //     },
          //   ),
          // ),
        ],
      ),
    );
  }
}
