import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/colors.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/gradients.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class WorkspaceBottomNav extends StatelessWidget {
  final String text;
  final IconData iconData;
  final VoidCallback onPress;
  const WorkspaceBottomNav({
    Key key,
    this.iconData,
    this.onPress,
    this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          GestureDetector(
            onTap: onPress,
            child: Container(
              width: 50.0,
              height: 50.0,
              decoration: BoxDecoration(
                gradient: ThemeGradients.linearGradientVertical,
                borderRadius: BorderRadius.all(Radius.circular(18)),
              ),
              child: Center(
                child: Icon(
                  iconData,
                  color: ThemeColors.white,
                  size: 30,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            text,
            style: ThemeText.quickLinkText,
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}
