import 'package:flutter/material.dart';
import 'colors.dart';

class ThemeText {
  static const TextStyle launchScreenTitle = TextStyle(
    color: ThemeColors.white,
    fontSize: 35.0,
    fontWeight: FontWeight.w600,
    fontFamily: 'Baloo Tamma 2'
  );

  static const TextStyle walkthroughTitle = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle walkthroughDescription = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle walkthroughButtonText = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle generalButtonText = TextStyle(
    color: ThemeColors.white,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle generalButtonTextGrey = TextStyle(
    color: ThemeColors.darkGrey,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle generalButtonTextPrimary = TextStyle(
    color: ThemeColors.primary,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle generalButtonTextPrimarySmall = TextStyle(
    color: ThemeColors.primary,
    fontSize: 10.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle headerLargeDark = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 64.0,
    height: 0.9,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle languageText = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 22.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle headerLargeWhite = TextStyle(
    color: ThemeColors.white,
    fontSize: 64.0,
    height: 0.9,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle languageTextWhite = TextStyle(
    color: ThemeColors.white,
    fontSize: 20.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle welcomeHeader = TextStyle(
    color: ThemeColors.white,
    fontSize: 24.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle searchHeaderTitle = TextStyle(
    color: ThemeColors.darkBlue,
    fontSize: 24.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle welcomeHeaderBold = TextStyle(
    color: ThemeColors.white,
    fontSize: 24.0,
    fontWeight: FontWeight.w900,
  );

  static const TextStyle welcomeHeaderDescription = TextStyle(
    color: ThemeColors.white,
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle authHeader = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 26.0,
    fontWeight: FontWeight.w300,
  );

  static const TextStyle authSubHeader = TextStyle(
    color: ThemeColors.darkBlue,
    fontSize: 20.0,
    fontWeight: FontWeight.w400,
  );

  static const TextStyle textboxTitle = TextStyle(
    color: ThemeColors.darkBlue,
    fontSize: 12.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle textboxPlaceholder = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 12.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle generalDescriptionLight = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 12.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle messageInfo = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 10.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle textInput = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle textInputWhite = TextStyle(
    color: ThemeColors.white,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle toggleButtonTextDark = TextStyle(
    color: ThemeColors.darkBlue,
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle toggleButtonTextlight = TextStyle(
    color: ThemeColors.white,
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle deviceLimit = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle deviceName = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 18.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle firmName = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle firmNameWhite = TextStyle(
    color: ThemeColors.white,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle customerNameMedium = TextStyle(
    color: ThemeColors.white,
    fontSize: 18.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle deviceIdLight = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle deviceIdDark = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle gstNoDark = TextStyle(
    color: ThemeColors.darkBlue,
    fontSize: 12.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle signOutDark = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 12.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle signOutWhite = TextStyle(
    color: ThemeColors.white,
    fontSize: 12.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle signOutSecondary = TextStyle(
    color: ThemeColors.secondary,
    fontSize: 12.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle googleText = TextStyle(
    color: ThemeColors.googleText,
    fontSize: 18.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle signInAgreementPrimary = TextStyle(
    color: ThemeColors.primary,
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle selectDateRange = TextStyle(
    color: ThemeColors.primary,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle signInAgreement = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 12.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle generalDescriptionBold = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 14.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle headdingBoldLight = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 18.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle primaryBold = TextStyle(
    color: ThemeColors.secondary,
    fontSize: 18.0,
    fontWeight: FontWeight.w800,
  );

  static const TextStyle secondaryTextDark = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle orderTotalAmount = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle orderTotalAmountGrey = TextStyle(
    color: ThemeColors.darkGrey,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle generalDescriptionDarkBold = TextStyle(
    color: ThemeColors.darkBlue,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle generalDescriptionBoldUnderline = TextStyle(
    color: ThemeColors.darkBlue,
    fontSize: 14.0,
    fontWeight: FontWeight.w700,
    decoration: TextDecoration.underline,
  );

  static const TextStyle generalTextBold = TextStyle(
    color: ThemeColors.darkBlue,
    fontSize: 14.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle generalTextBoldWhite = TextStyle(
    color: ThemeColors.white,
    fontSize: 14.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle notificationTitle = TextStyle(
    color: ThemeColors.white,
    fontSize: 12.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle notificationDescription = TextStyle(
    color: ThemeColors.white,
    fontSize: 10.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle cardTitle = TextStyle(
    color: ThemeColors.darkBlue,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle cardDescGreen = TextStyle(
    color: ThemeColors.successGreen,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle cardDescPrimary = TextStyle(
    color: ThemeColors.primary,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle cardDescSecondary = TextStyle(
    color: ThemeColors.secondary,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle cardDescRed = TextStyle(
    color: ThemeColors.errorRed,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle cardDescDarkBlue = TextStyle(
    color: ThemeColors.darkBlue,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle viewMore = TextStyle(
    color: ThemeColors.secondary,
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle secondaryHeader = TextStyle(
    color: ThemeColors.secondary,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle secondaryDarkHeader = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 20.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle backButtonText = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle pageHeaderTitleDark = TextStyle(
   fontFamily: 'Baloo Tamma 2',
    color: ThemeColors.darkBlue,
    fontSize: 20.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle pageHeaderTitleWhite = TextStyle(
   fontFamily: 'Baloo Tamma 2',
    color: ThemeColors.white,
    fontSize: 20.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle pageHeaderTitleSecondaryDark = TextStyle(
   fontFamily: 'Baloo Tamma 2',
    color: ThemeColors.secondaryDark,
    fontSize: 20.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle cancelText = TextStyle(
    color: ThemeColors.errorRed,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle bottomSheetText = TextStyle(
    color: ThemeColors.darkGrey,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle dashboardHeaderTitleWhite = TextStyle(
    color: ThemeColors.white,
    fontSize: 20.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle pageHeaderTitleSmallDark = TextStyle(
   fontFamily: 'Baloo Tamma 2',
    color: ThemeColors.darkBlue,
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle pageHeaderTitleSmallWhite = TextStyle(
   fontFamily: 'Baloo Tamma 2',
    color: ThemeColors.white,
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle dashboardTileText = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 10.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle dashboardTileValueRoyalblue = TextStyle(
    color: ThemeColors.royalBlue,
    fontSize: 24.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle dashboardTileValueInfoOrange = TextStyle(
    color: ThemeColors.infoOrange,
    fontSize: 24.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle dashboardTileValueSuccessGreen = TextStyle(
    color: ThemeColors.successGreen,
    fontSize: 24.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle dashboardTileValueErrorRed = TextStyle(
    color: ThemeColors.errorRed,
    fontSize: 24.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle dashboardTileValueRoyalblueMini = TextStyle(
    color: ThemeColors.royalBlue,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle dashboardTileValueInfoOrangeMini = TextStyle(
    color: ThemeColors.infoOrange,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle dashboardTileValueSuccessGreenMini = TextStyle(
    color: ThemeColors.successGreen,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle dashboardTileValueErrorRedMini = TextStyle(
    color: ThemeColors.errorRed,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle profileDisplayText = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle profileDisplayTextWhite = TextStyle(
    color: ThemeColors.white,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle profileName = TextStyle(
    color: ThemeColors.white,
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle quickLinkText = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 10.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle searchHeaderTitleWhite = TextStyle(
    color: ThemeColors.white,
    fontSize: 24.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle statsDesc = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 12.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle statsTextRed = TextStyle(
    color: ThemeColors.errorRed,
    fontSize: 24.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle statsTextRoyalBlue = TextStyle(
    color: ThemeColors.royalBlue,
    fontSize: 24.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle statsTextGreen = TextStyle(
    color: ThemeColors.green,
    fontSize: 24.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle statsTextOrrange = TextStyle(
    color: ThemeColors.infoOrange,
    fontSize: 24.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle forceUpdateText = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
  );
  
  static const TextStyle dashboardSubText = TextStyle(
    color: ThemeColors.white,
    fontSize: 12.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle dashboardWelcomeUser = TextStyle(
    color: ThemeColors.white,
    fontSize: 20.0,
    fontWeight: FontWeight.w500,
  );
  
  static const TextStyle filterText = TextStyle(
    color: ThemeColors.darkGrey,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle filterTextBold = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 16.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle orderTextDark = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle normalTextDark = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle salesText = TextStyle(
    color: ThemeColors.secondaryDark,
    fontSize: 22.0,
    fontWeight: FontWeight.w700,
  );

  static const TextStyle orderStatusGreen = TextStyle(
    color: ThemeColors.successGreen,
    fontSize: 12.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle orderStatusRed = TextStyle(
    color: ThemeColors.errorRed,
    fontSize: 12.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle itemTitleText = TextStyle(
    color: ThemeColors.darkGrey,
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
  );

  static const TextStyle itemValueText = TextStyle(
    color: ThemeColors.darkGrey,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle itemTotalText = TextStyle(
    color: ThemeColors.darkGrey,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
  );

  static const TextStyle itemTotalTextLight = TextStyle(
    color: ThemeColors.lightGrey,
    fontSize: 16.0,
    fontWeight: FontWeight.w600,
  );
}
