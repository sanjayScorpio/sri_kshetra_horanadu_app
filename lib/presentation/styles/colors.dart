import 'package:flutter/material.dart';

class ThemeColors {
  // static const primary = const Color(0xFF6C63FF);
  // static const secondary = const Color(0xFF6728FF);
  // static const secondaryDark = const Color(0xFF262166);
  // static const primary = const Color(0xFFFee9d7);
  static const primaryLight = const Color(0xFFFee9d7);
  static const primary = const Color(0xFFf9bf8f);
  static const secondary = const Color(0xFF34222e);
  static const secondaryDark = const Color(0xFF34222e);
  // static const secondary = const Color(0xFFf9bf8f);
  // static const secondaryDark = const Color(0xFF34222e);
  static const infoOrange = const Color(0xFFFF9F68);
  static const successGreen = const Color(0xFF51C360);
  static const errorRed = const Color(0xFFEB3349);
  static const skyBlue = const Color(0xFF00C6FF);
  static const violet = const Color(0xFFC76AE5);
  static const royalBlue = const Color(0xFF0072FF);
  static const lightGrey = const Color(0xFF9AA4B7);
  static const darkBlue = const Color(0xFF2F2E41);
  static const darkGrey = const Color(0xFF4C4C4C);
  static const white = const Color(0xFFF7FAFF);
  static const lightBlue = const Color(0xFFEDF6FF);
  static const safforn = const Color(0xFFFF6700);
  static const green = const Color(0xFF20BC2B);
  static const shadowGrey = const Color(0xFFDDDDDD);
  static const googleButtonBg = const Color(0xFFe9e9e9);
  static const googleText = const Color(0xFF757575);
}
