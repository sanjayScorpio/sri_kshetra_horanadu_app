import 'package:flutter/material.dart';
import 'colors.dart';

final lightThemeData = ThemeData(
  primarySwatch: Colors.blue,
  visualDensity: VisualDensity.adaptivePlatformDensity,
  fontFamily: "Poppins",
  primaryColor: ThemeColors.primary,
  accentColor: ThemeColors.secondary,
  // accentColorBrightness: Brightness.light,
  // primaryColorBrightness: Brightness.light
);

final darkThemeData = ThemeData(
  primarySwatch: Colors.blue,
  visualDensity: VisualDensity.adaptivePlatformDensity,
  fontFamily: "Poppins",
);
