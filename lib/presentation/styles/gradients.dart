import 'package:flutter/material.dart';
import 'colors.dart';

class ThemeGradients {
  static LinearGradient headerGradient = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: <Color>[
      ThemeColors.primary,
      ThemeColors.secondary,
    ],
  );

  static LinearGradient linearGradientVertical = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: <Color>[ThemeColors.primary, ThemeColors.secondary],
  );
}
