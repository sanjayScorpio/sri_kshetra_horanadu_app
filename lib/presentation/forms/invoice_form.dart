import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/utils/keyboard_action_config.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';
import 'package:file_picker/file_picker.dart';

class InvoiceForm extends StatefulWidget {
  InvoiceForm({Key key}) : super(key: key);

  @override
  _InvoiceFormState createState() => _InvoiceFormState();
}

class _InvoiceFormState extends State<InvoiceForm> {
  List<Map<String, String>> _attachments = [];
  String _filePath;

  void filterAttachments(name) {
    setState(() {
      _attachments =
          _attachments.where((element) => element['filename'] != name).toList();
    });
  }

  void getFilePath() async {
    try {
      String filePath1 = await FilePicker.getFilePath(type: FileType.any);
      // String filePath2 = await FilePicker.getFile
      if (filePath1 == '') {
        return;
      }
      print("File path: " + filePath1);
      setState(() {
        this._filePath = filePath1;
        this._attachments.add({
          'path': filePath1,
          'filename': filePath1 != null ? filePath1.split('/').last : '...'
        });
      });
      print("All file list $_attachments ");
    } on PlatformException catch (e) {
      print("Error while picking the file: " + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    double textBoxWidth =
        (MediaQuery.of(context).size.width / 2).toDouble() - 50;
    return KeyboardActions(
      disableScroll: true,
      autoScroll: false,
      config: buildConfig(context),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Date",
                  style: ThemeText.bottomSheetText,
                ),
                DatePickerButton(
                  textBoxWidth: textBoxWidth,
                )
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Invoice number",
                  style: ThemeText.bottomSheetText,
                ),
                SizedBox(
                  width: textBoxWidth,
                  child: CustomTextInput(
                      hint: "12345",
                      focusNode: nodeText1,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      textAlign: TextAlign.right,
                      prefix: Text(
                        "#",
                        style: TextStyle(color: ThemeColors.lightGrey),
                      )),
                )
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Amount",
                  style: ThemeText.bottomSheetText,
                ),
                SizedBox(
                  width: textBoxWidth,
                  child: CustomTextInput(
                    hint: "0.00",
                    focusNode: nodeText3,
                    keyboardType: TextInputType.number,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    textAlign: TextAlign.right,
                    prefix: Icon(
                      MdiIcons.currencyInr,
                      color: ThemeColors.lightGrey,
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Divider(
              thickness: 0.5,
              color: ThemeColors.lightGrey,
            ),
            attachmentList(_attachments),
            FlatButton(
              onPressed: getFilePath,
              child: Text("Add attachments"),
              textColor: ThemeColors.secondary,
            ),
            SizedBox(
              height: 20,
            ),
            PrimaryButton(title: "Save", onPress: null),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  Widget attachmentList(List<Map<String, String>> attachments) {
    return Container(
      constraints: BoxConstraints(maxHeight: 280),
      child: SingleChildScrollView(
        child: Column(
          children: [
            for (var item in attachments) ...{
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 55,
                    height: 55,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: item['filename'].toString().contains('.pdf')
                            ? NetworkImage(
                                'https://lh3.googleusercontent.com/proxy/tTbdGyUVMUYly1MHmoi4evqpF_pmjFXDu4ej2qOpiyS8jcMfUAgdPdH1x1P10v9N9hu6nZY1UF-7-dYCeN3HbUXucH_AAkVe-E0GQlTuCdJ2KGPBZgQEz7XixnRYgOtUGceIaRSp')
                            : AssetImage(item['path']),
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(12.0),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20, right: 5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            item['filename'],
                            style: ThemeText.generalTextBold,
                            overflow: TextOverflow.fade,
                            maxLines: 1,
                          ),
                          Text(
                            '5.5 MB',
                            style: ThemeText.signInAgreement,
                            overflow: TextOverflow.fade,
                            maxLines: 1,
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => filterAttachments(item['filename']),
                    child: Icon(
                      MdiIcons.deleteOutline,
                      color: ThemeColors.secondaryDark,
                      size: 30,
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              )
            }
          ],
        ),
      ),
    );
  }
}
