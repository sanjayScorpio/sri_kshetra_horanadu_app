import 'package:flutter/material.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:sri_kshetra_horanadu/utils/keyboard_action_config.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class EditPaymentFrom extends StatefulWidget {
  EditPaymentFrom({Key key}) : super(key: key);

  @override
  _EditPaymentFromState createState() => _EditPaymentFromState();
}

class _EditPaymentFromState extends State<EditPaymentFrom> {
  @override
  Widget build(BuildContext context) {
    double textBoxWidth =
        (MediaQuery.of(context).size.width / 2).toDouble() - 50;
    return KeyboardActions(
      disableScroll: true,
      autoScroll: false,
      config: buildConfig(context),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
        child: Column(
          children: [
            BottomModalRow(
              text: "Mode",
              child: Container(
                width: textBoxWidth,
                child: CustomTextInput(
                  focusNode: nodeText10,
                  hint: "Type eg : Cheque",
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            BottomModalRow(
              text: "Active",
              child: Switch(
                value: false,
                onChanged: (value) {
                  print("$value");
                },
              ),
            ),
            SizedBox(
              height: 10,
            ),
            PrimaryButton(title: "Save", onPress: null),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
