import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/payment_modes.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/dropDown_button.dart';
import 'package:sri_kshetra_horanadu/utils/keyboard_action_config.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class PaymentForm extends StatefulWidget {
  PaymentForm({Key key}) : super(key: key);

  @override
  _PaymentFormState createState() => _PaymentFormState();
}

class _PaymentFormState extends State<PaymentForm> {
  final List<String> _items = [
    'Cash',
    'NEFT',
    'UPI',
    'Others',
  ];
  @override
  Widget build(BuildContext context) {
    double textBoxWidth =
        (MediaQuery.of(context).size.width / 2).toDouble() - 50;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
      child: KeyboardActions(
        disableScroll: true,
        autoScroll: false,
        config: buildConfig(context),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Date",
                  style: ThemeText.bottomSheetText,
                ),
                DatePickerButton(textBoxWidth: textBoxWidth,)
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Amount",
                  style: ThemeText.bottomSheetText,
                ),
                SizedBox(
                  width: textBoxWidth,
                  child: CustomTextInput(
                    hint: "0.00",
                    keyboardType: TextInputType.number,
                    focusNode: nodeText1,
                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    textAlign: TextAlign.right,
                    prefix: Icon(
                      MdiIcons.currencyInr,
                      color: ThemeColors.lightGrey,
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Via",
                  style: ThemeText.bottomSheetText,
                ),
                SizedBox(
                  width: 5,
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () => paymetModesModal(context: context),
                      child: Icon(
                        MdiIcons.fileDocumentEditOutline,
                        color: ThemeColors.secondaryDark,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Container(
                      width: textBoxWidth,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        color: ThemeColors.primary,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 15.0,
                        ),
                        child: Theme(
                          data: Theme.of(context).copyWith(
                            canvasColor: ThemeColors.primary,
                          ),
                          child: CustomDropDownButton(
                            items: _items,
                            defaultText: "Select via",
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                "Description",
                style: ThemeText.bottomSheetText,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            CustomTextInput(
              focusNode: nodeText3,
              maxlines: 3,
            ),
            SizedBox(
              height: 20,
            ),
            PrimaryButton(title: "Save", onPress: null),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
