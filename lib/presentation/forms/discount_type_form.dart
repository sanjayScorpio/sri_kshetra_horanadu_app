import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

enum DiscountTypeConstant { amouunt, percentage }

class DiscountType extends StatefulWidget {
  DiscountType({Key key}) : super(key: key);

  @override
  _DiscountTypeState createState() => _DiscountTypeState();
}

class _DiscountTypeState extends State<DiscountType> {
  DiscountTypeConstant discountTypeConstant;

  @override
  void initState() {
    setState(() {
      this.discountTypeConstant = DiscountTypeConstant.percentage;
    });
    super.initState();
  }

  void setItemsType(DiscountTypeConstant discountTypeConstant) {
    setState(() {
      this.discountTypeConstant = discountTypeConstant;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  CustomToggleButtons(
                    "     ₹     ",
                    discountTypeConstant == DiscountTypeConstant.amouunt,
                    () => setItemsType(DiscountTypeConstant.amouunt),
                  ),
                  CustomToggleButtons(
                    "     %     ",
                    discountTypeConstant == DiscountTypeConstant.percentage,
                    () => setItemsType(DiscountTypeConstant.percentage),
                  ),
                ],
              ),
              Container(
                width: 150,
                child: CustomTextInput(
                  hint: "30.0",
                  textAlign: TextAlign.right,
                ),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          PrimaryButton(title: "Update", onPress: null)
        ],
      ),
    );
  }
}
