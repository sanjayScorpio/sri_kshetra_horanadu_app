import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/add_edit_payment_types.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class PaymentModesForm extends StatelessWidget {
  final List<String> itemList = [
    'NEFT',
    'UPI',
    'Card',
    'Cheque',
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
            child: BottomModalRow(
              text: "Cash",
              child: Container(),
            ),
          ),
          Divider(
            thickness: 1,
            color: ThemeColors.lightGrey,
          ),
          ListView.builder(
              shrinkWrap: true,
              itemCount: 1,
              itemBuilder: (BuildContext cont, int index) {
                return Column(
                  children: [
                    for (var item in itemList) ...[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
                        child: BottomModalRow(
                          text: item,
                          child: GestureDetector(
                            onTap: () =>
                                addEditPaymentModeModal(context: context),
                            child: Icon(
                              MdiIcons.squareEditOutline,
                              color: ThemeColors.secondaryDark,
                              size: 25,
                            ),
                          ),
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        color: ThemeColors.lightGrey,
                      )
                    ]
                  ],
                );
              }),
          SizedBox(
            height: 20,
          ),
          PrimaryButton(title: "Add new payment type", onPress: null),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
