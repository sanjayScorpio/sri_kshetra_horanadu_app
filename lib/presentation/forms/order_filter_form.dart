import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class OrderFilterForm extends StatefulWidget {
  OrderFilterForm({Key key}) : super(key: key);

  @override
  _OrderFilterFormState createState() => _OrderFilterFormState();
}

class _OrderFilterFormState extends State<OrderFilterForm> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: CheckboxListTile(
                  contentPadding: EdgeInsets.all(0),
                  title: Text(
                    "All orders",
                    style: ThemeText.filterText,
                  ),
                  value: true,
                  activeColor: ThemeColors.primary,
                  onChanged: (newValue) => {},
                  controlAffinity:
                      ListTileControlAffinity.leading, //  <-- leading Checkbox
                ),
              ),
              Expanded(
                child: CheckboxListTile(
                  contentPadding: EdgeInsets.all(0),
                  title: Text(
                    "Active orders",
                    style: ThemeText.filterText,
                  ),
                  activeColor: ThemeColors.primary,
                  // dense: true,

                  value: true,
                  onChanged: (newValue) => {},
                  controlAffinity:
                      ListTileControlAffinity.leading, //  <-- leading Checkbox
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: CheckboxListTile(
                  contentPadding: EdgeInsets.all(0),
                  title: Text(
                    "Completed orders",
                    style: ThemeText.filterText,
                  ),
                  value: true,
                  activeColor: ThemeColors.primary,
                  onChanged: (newValue) => {},
                  controlAffinity:
                      ListTileControlAffinity.leading, //  <-- leading Checkbox
                ),
              ),
              Expanded(
                child: CheckboxListTile(
                  contentPadding: EdgeInsets.all(0),
                  title: Text(
                    "Canceled orders",
                    style: ThemeText.filterText,
                  ),
                  activeColor: ThemeColors.primary,
                  // dense: true,
                  value: true,
                  onChanged: (newValue) => {},
                  controlAffinity:
                      ListTileControlAffinity.leading, //  <-- leading Checkbox
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Sort based on",
              style: ThemeText.filterTextBold,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Radio(
                      value: true,
                      groupValue: null,
                      onChanged: null,
                      toggleable: true,
                    ),
                    Text(
                      "Order date",
                      style: ThemeText.filterText,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Radio(
                      value: true,
                      groupValue: null,
                      onChanged: null,
                      toggleable: true,
                    ),
                    Text(
                      "Deliver date",
                      style: ThemeText.filterText,
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          PrimaryButton(title: "Apply filters", onPress: null),
          SizedBox(
            height: 15,
          ),
          Text(
            "Reset filters",
            style: ThemeText.signInAgreementPrimary,
          ),
        ],
      ),
    );
  }
}
