import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/add_invoice.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/add_payment.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/journal_entry.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class LedgerEntry extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
      child: Column(
        children: [
          JournalButton(
            title: "Add Invoice",
            onPress: () => addInvoiceModal(context: context),
            icon: MdiIcons.receipt,
          ),
          SizedBox(
            height: 20,
          ),
          JournalButton(
            title: "Add Payment",
            onPress: () => addPaymentModal(context: context),
            icon: MdiIcons.currencyInr,
          ),
          SizedBox(
            height: 20,
          ),
          JournalButton(
            title: "Journal Entry",
            onPress: () => journalEnteryModal(context: context),
            icon: MdiIcons.bookOutline,
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
