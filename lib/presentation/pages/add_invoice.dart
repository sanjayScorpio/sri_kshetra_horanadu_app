import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/forms/invoice_form.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

Future<dynamic> addInvoiceModal({BuildContext context}) {
  return bottomPageModal(
    context: context,
    titleText: "Add Invoice",
    isBackEnabled: true,
    isCancelEnabled: true,
    child: InvoiceForm(),
  );
}
