import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/customer_view_header.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/quick_action_links.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class CustomerView extends StatefulWidget {
  CustomerView({Key key}) : super(key: key);
  @override
  _CustomerViewState createState() => _CustomerViewState();
}

class _CustomerViewState extends State<CustomerView> {
  bool isExpanded = false;

  void _onPressExpand() {
    setState(() {
      isExpanded = !isExpanded;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            AnimatedContainer(
              duration: Duration(milliseconds: 800),
              child: CustomerViewHeader(
                isExpand: isExpanded,
                onPressExpand: () => _onPressExpand(),
              ),
            ),
            if (!isExpanded)
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0.0),
                        child: QuickActionLinks(),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: StatisticsWidet(
                          slips: "16",
                          slipsOnPress: () => {},
                          pending: "₹ 100000",
                          pendingOnPress: () => {},
                          orders: "60",
                          ordersOnPress: () => {},
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            if (isExpanded) ...{
              Container(
                decoration: BoxDecoration(
                  color: ThemeColors.white,
                  boxShadow: [
                    BoxShadow(
                      color: ThemeColors.lightGrey,
                      offset: Offset(0, 0),
                      blurRadius: 10.0,
                    ),
                  ],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  ),
                ),
                child: Center(
                  child: IconButton(
                    onPressed: _onPressExpand,
                    icon: Icon(
                      MdiIcons.chevronUp,
                      color: ThemeColors.darkGrey,
                      size: 30,
                    ),
                  ),
                ),
              )
            }
          ],
        ),
      ),
    );
  }
}
