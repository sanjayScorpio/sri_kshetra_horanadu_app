import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class OTPVerification extends StatefulWidget {
  OTPVerification({Key key}) : super(key: key);

  @override
  _OTPVerificationState createState() => _OTPVerificationState();
}

class _OTPVerificationState extends State<OTPVerification> {
  Timer _timer;
  int _start = 30;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_start < 1) {
            timer.cancel();
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: ThemeColors.white,
          child: Column(
            children: [
              RectangularGraphic(),
              Padding(
                padding: const EdgeInsets.only(left: 10, top: 30),
                child: GoBackButton(),
              ),
              Flexible(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Center(
                      child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 60,
                            ),
                            Text(
                              "Code verification",
                              style: ThemeText.authHeader,
                            ),
                            SizedBox(
                              height: 4.0,
                            ),
                            Text(
                              "Verify your authentication by entering code that we have sent you in the below box",
                              style: ThemeText.generalDescriptionLight,
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            OTPTextBox(),
                            SizedBox(
                              height: 20.0,
                            ),
                            PrimaryButton(
                                title: 'Verify OTP',
                                onPress: () => {
                                      AppRouter.goTo(AppRouter.accountSelection)
                                    }),
                            SizedBox(
                              height: 20.0,
                            ),
                            Center(
                              child: Text(
                                "Resend OTP ($_start)",
                                style: ThemeText.signInAgreementPrimary,
                              ),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
