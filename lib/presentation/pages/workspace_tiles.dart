import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/bottom_page_modal.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/workspace_bottom_nav.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/workspace_tiles/workspace.dart';

class WorkSpace extends StatefulWidget {
  WorkSpace({Key key}) : super(key: key);

  @override
  _WorkSpaceState createState() => _WorkSpaceState();
}

class _WorkSpaceState extends State<WorkSpace> {
  TextEditingController searchText = TextEditingController(text: "");
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: ThemeColors.secondary,
        title: Text("Workspace"),
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(15),
            bottomRight: Radius.circular(15),
          ),
        ),
        leading: GestureDetector(
          onTap: () => AppRouter.goBack(),
          child: Icon(
            MdiIcons.chevronLeft,
            color: ThemeColors.white,
          ),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    TextTile(),
                    SizedBox(
                      height: 8,
                    ),
                    PaymentTile(),
                    SizedBox(
                      height: 8,
                    ),
                    AudioTile(),
                    SizedBox(
                      height: 8,
                    ),
                    DocumentTile(),
                    SizedBox(
                      height: 8,
                    ),
                    ImageTile(),
                    SizedBox(
                      height: 8,
                    ),
                    OrderTile(),
                    SizedBox(
                      height: 8,
                    ),
                    InvoiceTile(),
                    SizedBox(
                      height: 8,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: ThemeColors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  offset: Offset(0, -2),
                  blurRadius: 30.0,
                ),
              ],
            ),
            padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                WorkspaceBottomNav(
                  iconData: MdiIcons.fileDocumentEditOutline,
                  onPress: () => {},
                  text: "Create Order",
                ),
                WorkspaceBottomNav(
                  iconData: MdiIcons.receipt,
                  onPress: () => {},
                  text: "Add invoice",
                ),
                WorkspaceBottomNav(
                  iconData: MdiIcons.currencyInr,
                  onPress: () => {},
                  text: "Receive payment",
                ),
                WorkspaceBottomNav(
                  iconData: MdiIcons.dotsHorizontal,
                  onPress: () => bottomPageModal(
                    context: context,
                    titleText: "More Options",
                    isBackEnabled: false,
                    isCancelEnabled: true,
                    child: moreOptions(),
                  ),
                  text: "More options",
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget moreOptions() {
    return Padding(
      padding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          WorkspaceBottomNav(
            iconData: MdiIcons.commentTextOutline,
            onPress: () => {},
            text: "Add Text",
          ),
          WorkspaceBottomNav(
            iconData: MdiIcons.fileImageOutline,
            onPress: () => {},
            text: "Add Image",
          ),
          WorkspaceBottomNav(
            iconData: MdiIcons.microphoneOutline,
            onPress: () => {},
            text: "Add Audio",
          ),
          WorkspaceBottomNav(
            iconData: MdiIcons.filePdfOutline,
            onPress: () => {},
            text: "Add Files",
          ),
        ],
      ),
    );
  }
}
