import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class ForgotPassword extends StatelessWidget {
  ForgotPassword({Key key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Container(
            color: ThemeColors.white,
            child: Column(
              children: [
                RectangularGraphic(),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 10,
                    top: 30,
                  ),
                  child: GoBackButton(),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Container(
                        alignment: Alignment.center,
                        child: Form(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 40,
                              ),
                              Text(
                                "Forgot Password",
                                style: ThemeText.authHeader,
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              RichText(
                                text: TextSpan(
                                  text: 'Please enter your ',
                                  style: ThemeText.generalDescriptionLight,
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: 'E-Mail ',
                                      style:
                                          ThemeText.generalDescriptionDarkBold,
                                    ),
                                    TextSpan(
                                      text: 'or ',
                                      style: ThemeText.generalDescriptionLight,
                                    ),
                                    TextSpan(
                                      text: 'Mobile number',
                                      style:
                                          ThemeText.generalDescriptionDarkBold,
                                    ),
                                    TextSpan(
                                      text:
                                          '. We will send you password reset link',
                                      style: ThemeText.generalDescriptionLight,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 30.0,
                              ),
                              CustomTextInput(
                                hint: "E-mail / Phone number",
                                keyboardType: TextInputType.emailAddress,
                                validator: (String value) {
                                  return value.isEmpty
                                      ? 'Cannot be empty'
                                      : null;
                                },
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              PrimaryButton(
                                  title: 'Continue',
                                  onPress: () => {
                                        if (_formKey.currentState.validate())
                                          {
                                            AppRouter.goTo(
                                                AppRouter.resetPassword)
                                          }
                                        else
                                          {
                                            Scaffold.of(context).showSnackBar(
                                                SnackBar(
                                                    content: Text('Error')))
                                          },
                                      }),
                              SizedBox(
                                height: 20.0,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
