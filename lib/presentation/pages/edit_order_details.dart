import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/edit_discount.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class EditOrderDetails extends StatefulWidget {
  EditOrderDetails({Key key}) : super(key: key);

  @override
  _EditOrderDetailsState createState() => _EditOrderDetailsState();
}

class _EditOrderDetailsState extends State<EditOrderDetails> {
  bool showImage = false;

  int itemCount = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void handleImageContainer() {
    setState(() {
      showImage = !this.showImage;
    });
  }

  void handleItemRemove() {
    setState(() {
      itemCount -= 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    double textBoxWidth =
        (MediaQuery.of(context).size.width / 2).toDouble() - 40;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ThemeColors.white,
          centerTitle: true,
          title: Text(
            "Edit order details",
            style: ThemeText.pageHeaderTitleDark,
          ),
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              MdiIcons.close,
              color: ThemeColors.secondaryDark,
            ),
            onPressed: () => AppRouter.goBack(),
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Column(
                    children: [
                      ItemCustomerSection(),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Date",
                            style: ThemeText.bottomSheetText,
                          ),
                          DatePickerButton(
                            textBoxWidth: textBoxWidth,
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Deliver on",
                            style: ThemeText.bottomSheetText,
                          ),
                          DatePickerButton(
                            textBoxWidth: textBoxWidth,
                            isTimeRequired: true,
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Item list", style: ThemeText.bottomSheetText),
                            FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              onPressed: () {
                                setState(() {
                                  itemCount += 1;
                                });
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Text('Add item'),
                              ),
                              color: ThemeColors.primary,
                              textColor: ThemeColors.white,
                            )
                          ],
                        ),
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      for (var i = 0; i < itemCount; i++) ...{
                        EditItemCard(
                          deleteItem: handleItemRemove,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Divider(
                          thickness: 1,
                        ),
                      },
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              decoration: BoxDecoration(
                color: ThemeColors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                    color: ThemeColors.shadowGrey,
                    offset: Offset(2, -5),
                    blurRadius: 30.0,
                  ),
                ],
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: Row(
                      children: [
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15.0),
                                child: Text(
                                  "Total",
                                  style: ThemeText.orderTotalAmount,
                                ),
                              ),
                              Text(
                                "₹5000",
                                style: ThemeText.primaryBold,
                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () => {
                            bottomPageModal(
                              context: context,
                              isTitleRequired: false,
                              isBackEnabled: false,
                              // isCancelEnabled: true,
                              child: totalList(context),
                            )
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Icon(
                              MdiIcons.informationOutline,
                              color: ThemeColors.darkGrey,
                              size: 30,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  PrimaryButton(title: 'update', onPress: () => {}),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget totalList(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Stack(
            children: [
              Expanded(
                flex: 3,
                child: Center(
                  child: Text(
                    "Price break up",
                    style: ThemeText.itemTotalText,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(bottom: 20),
                  child: GestureDetector(
                    onTap: () => AppRouter.goBack(),
                    child: Icon(
                      MdiIcons.closeCircleOutline,
                      size: 30,
                    ),
                  ),
                ),
              )
            ],
          ),
          itemTotalRow(
            text: "Sub Total",
            value: "₹ 50,000",
            isBold: true,
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Text(
                    "Discount  (5%)",
                    style: ThemeText.itemTotalText,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  GestureDetector(
                    onTap: () => editDiscountModal(context: context),
                    child: Icon(
                      MdiIcons.pencil,
                      color: ThemeColors.secondaryDark,
                    ),
                  ),
                ],
              ),
              Text(
                "₹ 50,000",
                style: ThemeText.itemTotalText,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  "Misc  (12% GST)",
                  style: ThemeText.itemTotalTextLight,
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                flex: 1,
                child: TextField(
                  style: ThemeText.textInput,
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  textAlign: TextAlign.right,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(bottom: -10),
                    prefix: Text("₹  "),
                    hintStyle: TextStyle(
                      color: ThemeColors.lightGrey,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          itemTotalRow(
            text: "CGST",
            value: "₹ 6500",
            isBold: false,
          ),
          SizedBox(
            height: 10,
          ),
          itemTotalRow(
            text: "SGST",
            value: "₹ 6500",
            isBold: false,
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            thickness: 1,
          ),
          itemTotalRow(
            text: "Total",
            value: "₹ 9596500",
            isBold: true,
          ),
        ],
      ),
    );
  }

  Widget itemTotalRow({text, value, isBold}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          text,
          style: isBold == true
              ? ThemeText.itemTotalText
              : ThemeText.itemTotalTextLight,
        ),
        Text(
          value,
          style: isBold == true
              ? ThemeText.itemTotalText
              : ThemeText.itemTotalTextLight,
        ),
      ],
    );
  }
}
