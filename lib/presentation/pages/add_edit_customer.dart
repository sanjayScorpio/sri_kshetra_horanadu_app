import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/utils/keyboard_action_config.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

enum CustomerType { person, firm }

class CustomerForm extends StatefulWidget {
  CustomerForm({Key key}) : super(key: key);

  @override
  _CustomerFormState createState() => _CustomerFormState();
}

class _CustomerFormState extends State<CustomerForm> {
  CustomerType customerType = CustomerType.person;
  final _formKey = GlobalKey<FormState>();

  bool showImage = false;

  TextEditingController nameTextController = TextEditingController();
  TextEditingController codeTextController = TextEditingController();
  TextEditingController gstTextController = TextEditingController();
  TextEditingController contactPersonTextController = TextEditingController();
  TextEditingController mobileTextController = TextEditingController();
  TextEditingController emailTextController = TextEditingController();
  TextEditingController addressTextController = TextEditingController();
  TextEditingController cityTextController = TextEditingController();
  TextEditingController pincodeTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    nameTextController.dispose();
    codeTextController.dispose();
    gstTextController.dispose();
    contactPersonTextController.dispose();
    mobileTextController.dispose();
    emailTextController.dispose();
    addressTextController.dispose();
    cityTextController.dispose();
    pincodeTextController.dispose();
    super.dispose();
  }

  void handleImageContainer() {
    setState(() {
      showImage = !this.showImage;
    });
  }

  void setCustomerType(CustomerType customerType) {
    setState(() {
      this.customerType = customerType;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ThemeColors.white,
          centerTitle: true,
          title: Text(
            "Add Customer",
            style: ThemeText.pageHeaderTitleDark,
          ),
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              MdiIcons.close,
              color: ThemeColors.secondaryDark,
            ),
            onPressed: () => AppRouter.goBack(),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () => {
                  if (_formKey.currentState.validate())
                    AppRouter.goTo(AppRouter.itemsForm)
                  else
                    Scaffold.of(context)
                        .showSnackBar(SnackBar(content: Text('Error'))),
                },
                child: Center(
                  child: Text(
                    "save",
                    style: ThemeText.signOutDark,
                  ),
                ),
              ),
            )
          ],
        ),
        body: KeyboardActions(
          disableScroll: true,
          autoScroll: false,
          config: buildConfig(context),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Container(
                        decoration: BoxDecoration(
                          color: ThemeColors.lightBlue,
                          borderRadius: BorderRadius.all(
                            Radius.circular(12),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                child: CustomToggleButtons(
                                  "Person",
                                  customerType == CustomerType.person,
                                  () => setCustomerType(CustomerType.person),
                                ),
                              ),
                              SizedBox(
                                width: 20.0,
                              ),
                              Expanded(
                                child: CustomToggleButtons(
                                  "Firm",
                                  customerType == CustomerType.firm,
                                  () => setCustomerType(CustomerType.firm),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    if (showImage == true)
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 30),
                        child: Center(
                          child: Stack(
                            children: [
                              Container(
                                width: 120,
                                height: 120,
                                decoration: BoxDecoration(
                                  color: ThemeColors.lightBlue,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(30),
                                  ),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        'https://cdn.pixabay.com/photo/2014/02/27/16/10/tree-276014__340.jpg'),
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xFFd6d6d6),
                                      offset: Offset(2, 2),
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: 10,
                                right: 10,
                                child: GestureDetector(
                                  onTap: () => handleImageContainer(),
                                  child: Container(
                                    padding: EdgeInsets.all(2),
                                    decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0xFFd6d6d6),
                                            offset: Offset(2, 2)),
                                      ],
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Icon(
                                      MdiIcons.close,
                                      size: 18,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    else
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 30),
                        child: GestureDetector(
                          onTap: () => handleImageContainer(),
                          child: Center(
                            child: Container(
                              width: 120,
                              height: 120,
                              decoration: BoxDecoration(
                                border:
                                    Border.all(color: ThemeColors.secondary),
                                color: ThemeColors.lightBlue,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(30),
                                ),
                              ),
                              child: Icon(
                                MdiIcons.image,
                                color: ThemeColors.lightGrey,
                                size: 40,
                              ),
                            ),
                          ),
                        ),
                      ),
                    CustomTextInput(
                      labelText: customerType == CustomerType.firm
                          ? "Firm name"
                          : "Name",
                      hint: customerType == CustomerType.firm
                          ? "Enter firm name"
                          : "Enter person name",
                      controller: nameTextController,
                      focusNode: nodeText1,
                      validator: (String value) {
                        return value.isEmpty ? 'Name Can\'t be empty' : null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextInput(
                      labelText: "Code",
                      hint: "Enter Code",
                      focusNode: nodeText2,
                      controller: codeTextController,
                      keyboardType: TextInputType.number,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    if (customerType == CustomerType.firm) ...[
                      CustomTextInput(
                        labelText: "GSTIN",
                        hint: "Optional",
                        focusNode: nodeText3,
                        controller: gstTextController,
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      CustomTextInput(
                        labelText: "Contact Person",
                        hint: "Name",
                        focusNode: nodeText4,
                        controller: contactPersonTextController,
                        validator: (String value) {
                          return value.isEmpty ? 'Name is Required' : null;
                        },
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                    ],
                    CustomTextInput(
                      labelText: "Mobile",
                      hint: "9585865856",
                      focusNode: nodeText5,
                      controller: mobileTextController,
                      prefix: Text(
                        '+91 ',
                        style: ThemeText.textInput,
                      ),
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.next,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(
                          RegExp(r'^\d{0,10}'),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    CustomTextInput(
                      labelText: "E-Mail",
                      focusNode: nodeText6,
                      controller: emailTextController,
                      hint: "someone@yourmail.com",
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextInput(
                      labelText: "Address",
                      focusNode: nodeText7,
                      controller: addressTextController,
                      hint: "#No, street, Landmark",
                      maxlines: 3,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextInput(
                      labelText: "City",
                      focusNode: nodeText8,
                      hint: "Bengaluru",
                      controller: cityTextController,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextInput(
                      labelText: "Pincode",
                      hint: "525252",
                      focusNode: nodeText9,
                      controller: pincodeTextController,
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.done,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(
                          RegExp(r'^\d{0,6}'),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
