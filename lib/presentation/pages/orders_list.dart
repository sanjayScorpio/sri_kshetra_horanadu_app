import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/order_card.dart';

class OrdersList extends StatefulWidget {
  OrdersList({Key key}) : super(key: key);

  @override
  _OrdersListState createState() => _OrdersListState();
}

class _OrdersListState extends State<OrdersList> {
  TextEditingController searchText = TextEditingController(text: "");
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: ThemeColors.secondary,
        title: Text("Orders"),
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(15),
            bottomRight: Radius.circular(15),
          ),
        ),
        leading: GestureDetector(
          onTap: () => AppRouter.goBack(),
          child: Icon(
            MdiIcons.chevronLeft,
            color: ThemeColors.white,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 15.0),
            child: GestureDetector(
              onTap: () => AppRouter.goTo(AppRouter.editOrderDetails),
              child: Icon(
                MdiIcons.filterOutline,
                color: ThemeColors.white,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Icon(
              MdiIcons.sort,
              color: ThemeColors.white,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            children: [
              for (var i = 0; i < 5; i++) ...{
                OrderCard(),
                SizedBox(
                  height: 8,
                )
              },
            ],
          ),
        ),
      ),
    );
  }
}
