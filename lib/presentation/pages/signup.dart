import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/custom_text_input.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/google_button.dart';
import 'package:sri_kshetra_horanadu/utils/keyboard_action_config.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/primary_button.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/rectangular_graphic.dart';

class SignUp extends StatefulWidget {
  SignUp({Key key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: ThemeColors.white,
          child: Column(
            children: [
              RectangularGraphic(),
              Expanded(
                child: KeyboardActions(
                  disableScroll: true,
                  autoScroll: false,
                  config: buildConfig(context),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20.0,
                      vertical: 5.0,
                    ),
                    child: SingleChildScrollView(
                      child: Container(
                        child: Form(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 40.0,
                              ),
                              Text(
                                "Let's Create, An Account",
                                style: ThemeText.authHeader,
                              ),
                              Text(
                                "Enter your details below",
                                style: ThemeText.authSubHeader,
                              ),
                              SizedBox(
                                height: 30.0,
                              ),
                              Center(
                                child: GoogleAuthButton(
                                  "Sign up with Google",
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Center(
                                  child: Text(
                                    "OR",
                                    style: ThemeText.generalDescriptionBold,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              CustomTextInput(
                                labelText: "Name",
                                focusNode: nodeText1,
                                hint: "Prakash",
                                keyboardType: TextInputType.name,
                                validator: (String value) {
                                  return value.isEmpty
                                      ? 'Name is Required'
                                      : null;
                                },
                                textInputAction: TextInputAction.next,
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              CustomTextInput(
                                labelText: "Mobile",
                                focusNode: nodeText2,
                                hint: "9585865856",
                                prefix: Text(
                                  '+91 ',
                                  style: ThemeText.textInput,
                                ),
                                keyboardType: TextInputType.phone,
                                textInputAction: TextInputAction.next,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.allow(
                                    RegExp(r'^\d{0,10}'),
                                  )
                                ],
                                validator: (String value) {
                                  return value.isEmpty
                                      ? 'Invalid mobile number'
                                      : null;
                                },
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              CustomTextInput(
                                labelText: "E-mail",
                                hint: "someone@yourmail.com",
                                focusNode: nodeText3,
                                keyboardType: TextInputType.emailAddress,
                                textInputAction: TextInputAction.done,
                                validator: (String value) {
                                  return value.isEmpty ? 'Invalid Email' : null;
                                },
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              Center(
                                child: RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(
                                    text:
                                        'By registering you confirm that you accept our \n',
                                    style: ThemeText.signInAgreement,
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: 'Terms of Use',
                                        style: ThemeText.signInAgreementPrimary,
                                      ),
                                      TextSpan(
                                        text: ' and ',
                                        style: ThemeText.signInAgreement,
                                      ),
                                      TextSpan(
                                        text: 'Privacy Policy',
                                        style: ThemeText.signInAgreementPrimary,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              PrimaryButton(
                                  title: 'Register',
                                  onPress: () => {
                                        if (_formKey.currentState.validate())
                                          {
                                            AppRouter.goTo(
                                                AppRouter.createPassword)
                                          }
                                        else
                                          {
                                            Scaffold.of(context).showSnackBar(
                                                SnackBar(
                                                    content: Text('Error')))
                                          },
                                      }),
                              SizedBox(
                                height: 15.0,
                              ),
                              Center(
                                child: RichText(
                                  text: TextSpan(
                                    text: 'Already have an Account ? ',
                                    style: ThemeText.generalDescriptionLight,
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: 'Sign In',
                                          style: ThemeText
                                              .generalDescriptionBoldUnderline,
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () => {
                                                  AppRouter.goTo(
                                                      AppRouter.signIn)
                                                }),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
