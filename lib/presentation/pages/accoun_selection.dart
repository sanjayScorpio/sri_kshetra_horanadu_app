import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class AccountSelection extends StatelessWidget {
  const AccountSelection({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: FullPageRectangularGraphic(
            SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 30.0, horizontal: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(left: 50),
                            alignment: Alignment.center,
                            child: Text(
                              "Select Account",
                              style: ThemeText.pageHeaderTitleWhite,
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () => {AppRouter.goTo(AppRouter.signIn)},
                          child: Text(
                            "Sign out",
                            style: ThemeText.signOutWhite,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    width: 145,
                    height: 145,
                    decoration: BoxDecoration(
                      border: Border.all(color: ThemeColors.white, width: 3),
                      borderRadius: BorderRadius.all(Radius.circular(55.0)),
                    ),
                    child: Center(
                      child: Container(
                        width: 120,
                        height: 120,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                  'https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80')),
                          borderRadius: BorderRadius.all(
                            Radius.circular(40.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Randeep Hodda",
                    style: ThemeText.profileDisplayTextWhite,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  ...List.generate(
                    5,
                    (index) => Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: AccountSelectionCard(),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 15.0),
                    child: PrimaryButtonWhite(
                      title: "Add Account",
                      onPress: () => {AppRouter.goTo(AppRouter.deviceLimit)},
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
