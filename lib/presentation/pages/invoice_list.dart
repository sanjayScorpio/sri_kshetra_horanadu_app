import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/bottom_page_modal.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/gradient_search_bar.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/invoice_card.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class InvoiceList extends StatefulWidget {
  InvoiceList({Key key}) : super(key: key);

  @override
  _InvoiceListState createState() => _InvoiceListState();
}

class _InvoiceListState extends State<InvoiceList> {
  TextEditingController searchText = TextEditingController(text: "");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: ThemeColors.secondary,
        title: Text("Invoices"),
        elevation: 0,
        leading: GestureDetector(
          onTap: () => AppRouter.goBack(),
          child: Icon(
            MdiIcons.chevronLeft,
            color: ThemeColors.white,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 15.0),
            child: GestureDetector(
              onTap: () => bottomPageModal(
                context: context,
                titleText: "Filters",
                isBackEnabled: false,
                isCancelEnabled: true,
                child: totalList(context),
              ),
              child: Icon(
                MdiIcons.filterOutline,
                color: ThemeColors.white,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Icon(
              MdiIcons.sort,
              color: ThemeColors.white,
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          SafeArea(
            child: Container(
              decoration: BoxDecoration(
                color: ThemeColors.secondary,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 10, 20, 15),
                child: Column(
                  children: [
                    GradientSearchBar(
                      controller: searchText,
                      hint: "search Invoice Number",
                      isTransparent: true,
                      prefixIcon: MdiIcons.magnify,
                      suffixIcon: GestureDetector(
                        onTap: () => {searchText.clear()},
                        child: Icon(
                          MdiIcons.close,
                          color: ThemeColors.white,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          text: 'From ',
                          style: ThemeText.notificationTitle,
                          children: <TextSpan>[
                            TextSpan(
                              text: '12/12/2020',
                              style: ThemeText.generalTextBoldWhite,
                            ),
                            TextSpan(
                              text: '   To ',
                              style: ThemeText.notificationTitle,
                            ),
                            TextSpan(
                              text: '12/12/2021',
                              style: ThemeText.generalTextBoldWhite,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  children: [
                    for (var i = 0; i < 5; i++) ...{
                      InvoiceCard(),
                      SizedBox(
                        height: 8,
                      )
                    },
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget totalList(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.all(10.0),
    child: Column(
      children: [
        DateRangePicker(),
        SizedBox(
          height: 15,
        ),
        PrimaryButton(title: "Apply filters", onPress: () {}),
        SizedBox(
          height: 15,
        ),
        Text(
          "Reset filters",
          style: ThemeText.signInAgreementPrimary,
        ),
      ],
    ),
  );
}
