import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/forms/order_filter_form.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

Future<dynamic> orderFiltersModal({
  BuildContext context,
  bool allOrders,
  bool completedOrders,
  bool activeOrders,
  bool canceledOrders,
  VoidCallback onPressAllOrders,
  VoidCallback onPressCompletedOrders,
  VoidCallback onPressActiveOrders,
  VoidCallback onPressCanceledOrders,
}) {
  return bottomPageModal(
    context: context,
    titleText: "Add Invoice",
    isCancelEnabled: true,
    child: OrderFilterForm(),
  );
}
