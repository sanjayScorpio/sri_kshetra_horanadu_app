import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sri_kshetra_horanadu/presentation/forms/payment_form.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

Future<dynamic> addPaymentModal({
  BuildContext context,
  VoidCallback onChange,
}) {
  return bottomPageModal(
      context: context,
      titleText: "Add Payment",
      isBackEnabled: true,
      isCancelEnabled: true,
      child: PaymentForm());
}
