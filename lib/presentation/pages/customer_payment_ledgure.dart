import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class CustomerPaymentLedgure extends StatefulWidget {
  CustomerPaymentLedgure({Key key}) : super(key: key);

  @override
  _CustomerPaymentLedgureState createState() => _CustomerPaymentLedgureState();
}

class _CustomerPaymentLedgureState extends State<CustomerPaymentLedgure> {
  bool _isVisible = false;
  int noOfItems = 10;
  void _showSnackBar(BuildContext context, String text) {
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(text)));
  }

  ScrollController _hideButtonController = ScrollController();

  hideScrollButton() {
    if (_hideButtonController.position.atEdge) {
      if (_hideButtonController.position.pixels == 0) {
        if (_isVisible == true) {
          setState(() {
            _isVisible = false;
          });
        }
      }
    }
  }

  @override
  initState() {
    super.initState();
    _hideButtonController.addListener(() {
      if (_hideButtonController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (_isVisible == false) {
          setState(() {
            _isVisible = true;
          });
        }
      } else {
        hideScrollButton();
      }
      if (_hideButtonController.position.atEdge) {
        if (_hideButtonController.position.pixels != 0) {
          Timer(Duration(seconds: 0), viewMoreItems());
        }
      }
    });
  }

  viewMoreItems() {
    setState(() {
      if (noOfItems < 30) {
        noOfItems += 10;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double textBoxWidth =
        (MediaQuery.of(context).size.width / 2).toDouble() - 45;
    return Scaffold(
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 50),
        child: Visibility(
          visible: _isVisible,
          child: FloatingActionButton(
            backgroundColor: ThemeColors.secondary,
            mini: true,
            child: Icon(
              MdiIcons.chevronDown,
              color: ThemeColors.white,
            ),
            onPressed: () => {
              _hideButtonController.animateTo(
                0.0,
                duration: Duration(seconds: 1),
                curve: Curves.easeInOut,
              ),
              hideScrollButton()
            },
          ),
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: ThemeColors.secondary,
        title: Text("Jagdeesh vasudev"),
        elevation: 0,
        leading: Icon(
          MdiIcons.chevronLeft,
          color: ThemeColors.white,
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Icon(
              MdiIcons.downloadOutline,
              color: ThemeColors.white,
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                color: ThemeColors.secondary,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15),
                )),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        DatePickerButtonWhite(
                          textBoxWidth: textBoxWidth,
                          text: "From",
                        ),
                        DatePickerButtonWhite(
                          textBoxWidth: textBoxWidth,
                          text: "To",
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () => {},
                    child: Text(
                      "Clear",
                      style: ThemeText.signOutWhite,
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            child: SingleChildScrollView(
                controller: _hideButtonController,
                reverse: true,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 0),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Stack(
                          children: [
                            Positioned(
                              left: 8,
                              child: Text(
                                "12/12/2020",
                                style: ThemeText.generalButtonTextGrey,
                              ),
                            ),
                            Center(
                              child: Text(
                                "Opening balance",
                                style: ThemeText.generalButtonTextPrimary,
                              ),
                            ),
                            Positioned(
                              right: 8,
                              child: Text(
                                "+5000",
                                style: ThemeText.cardDescGreen,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      for (var i = noOfItems; i > 0; i--) ...{
                        Slidable(
                          actionPane: SlidableDrawerActionPane(),
                          secondaryActions: <Widget>[
                            IconSlideAction(
                              closeOnTap: true,
                              color: ThemeColors.successGreen,
                              icon: MdiIcons.squareEditOutline,
                              foregroundColor: ThemeColors.white,
                              onTap: () => _showSnackBar(context, 'Edit'),
                            ),
                            IconSlideAction(
                              closeOnTap: true,
                              color: ThemeColors.errorRed,
                              icon: MdiIcons.deleteOutline,
                              onTap: () => _showSnackBar(context, 'Delete'),
                            ),
                          ],
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Stack(
                                  children: [
                                    Positioned(
                                      left: 8,
                                      child: Text(
                                        "$i/12/2020",
                                        style: ThemeText.generalButtonTextGrey,
                                      ),
                                    ),
                                    Center(
                                      child: Text(
                                        i % 2 == 0 ? "Payment" : "invoice",
                                        style:
                                            ThemeText.generalButtonTextPrimary,
                                      ),
                                    ),
                                    Positioned(
                                      right: 8,
                                      child: Text(
                                        i % 2 == 0 ? "-5000" : "+5000",
                                        style: i % 2 == 0
                                            ? ThemeText.cardDescRed
                                            : ThemeText.cardDescGreen,
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    i % 2 == 0
                                        ? "If I do not go from place"
                                        : "If I do not go from place to place and offer the message that the world is unreal, people will not hear",
                                    style: ThemeText.generalDescriptionLight,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Divider(
                          thickness: 1,
                        ),
                      }
                    ],
                  ),
                )),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 10),
            child: CustomerPaymentTextBox(),
          )
        ],
      ),
    );
  }
}
