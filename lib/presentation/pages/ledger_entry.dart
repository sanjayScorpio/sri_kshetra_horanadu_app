import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/forms/ledger_entry_form.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

Future<dynamic> ledgerEnteryModal({BuildContext context}) {
  return bottomPageModal(
    context: context,
    titleText: "Ledger Entry",
    isCancelEnabled: true,
    child: LedgerEntry(),
  );
}
