import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/forms/journal_entry_form.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

Future<dynamic> journalEnteryModal({BuildContext context}) {
  return bottomPageModal(
    context: context,
    titleText: "Journal Entry",
    isCancelEnabled: true,
    isBackEnabled: true,
    child: JournalEntryForm(),
  );
}
