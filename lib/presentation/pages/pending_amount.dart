import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class PendingAmount extends StatefulWidget {
  PendingAmount({Key key}) : super(key: key);

  @override
  _PendingAmountState createState() => _PendingAmountState();
}

class _PendingAmountState extends State<PendingAmount> {
  TextEditingController searchText = TextEditingController(text: "");
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: ThemeColors.secondary,
        title: Text("Pending amount"),
        elevation: 0,
        leading: GestureDetector(
          onTap: () => AppRouter.goBack(),
          child: Icon(
            MdiIcons.chevronLeft,
            color: ThemeColors.white,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Icon(
              MdiIcons.dotsVertical,
              color: ThemeColors.white,
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          SafeArea(
            child: Container(
              decoration: BoxDecoration(
                color: ThemeColors.secondary,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 10, 20, 25),
                child: GradientSearchBar(
                  controller: searchText,
                  hint: "search Customer name",
                  isTransparent: true,
                  prefixIcon: MdiIcons.magnify,
                  suffixIcon: GestureDetector(
                    onTap: () => {searchText.clear()},
                    child: Icon(
                      MdiIcons.close,
                      color: ThemeColors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  children: [
                    PendingPaymentCard(
                      image: NetworkImage(
                        "https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80",
                      ),
                      title: "New Microphone Set",
                      tag: "Phone",
                      amount: "+ ₹5000",
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    PendingPaymentCard(
                      image: NetworkImage(
                        "https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80",
                      ),
                      title: "New Microphone Set",
                      tag: "Phone",
                      amount: "- ₹5000",
                      isInvoice: true,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    PendingPaymentCard(
                      image: NetworkImage(
                        "https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80",
                      ),
                      title: "New Microphone Set",
                      tag: "Phone",
                      amount: "+ ₹500000",
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    PendingPaymentCard(
                      image: NetworkImage(
                        "https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80",
                      ),
                      title: "New Microphone Set",
                      tag: "Phone",
                      amount: "- ₹5000",
                      isInvoice: true,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    PendingPaymentCard(
                      image: NetworkImage(
                        "https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80",
                      ),
                      title: "New Microphone Set",
                      tag: "Phone",
                      amount: "+ ₹5000",
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    PendingPaymentCard(
                      image: NetworkImage(
                        "https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80",
                      ),
                      title: "New Microphone Set",
                      tag: "Phone",
                      amount: "- ₹5000",
                      isInvoice: true,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    PendingPaymentCard(
                      image: NetworkImage(
                        "https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80",
                      ),
                      title: "New Microphone Set",
                      tag: "Phone",
                      amount: "+ ₹5000",
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    PendingPaymentCard(
                      image: NetworkImage(
                        "https://images.unsplash.com/photo-1571839154183-6bb84a567903?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=812&q=80",
                      ),
                      title: "New Microphone Set",
                      tag: "Phone",
                      amount: "- ₹5000",
                      isInvoice: true,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
