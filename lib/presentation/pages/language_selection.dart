import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

enum LanguageList { english, hindi, kannada }

class LanguageSelection extends StatefulWidget {
  LanguageSelection({Key key}) : super(key: key);

  @override
  _LanguageSelectionState createState() => _LanguageSelectionState();
}

class _LanguageSelectionState extends State<LanguageSelection> {
  // bool isActive = false;
  static Map<String, Map<String, String>> _localizedValues = {
    'english': {'title': 'A', 'lang': 'English', 'locale': 'en'},
    'hindi': {'title': 'अ', 'lang': 'हिंदी', 'locale': 'hi'},
    'kannada': {'title': 'ಅ', 'lang': 'ಕನ್ನಡ', 'locale': 'kn'},
  };

  LanguageList languageList;

  void setLanguage(LanguageList languageItem) {
    setState(() {
      this.languageList = languageItem;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: Text(
                "Select Language",
                style: ThemeText.secondaryDarkHeader,
              ),
            ),
            Expanded(
              child: GridView.count(
                padding: EdgeInsets.all(20),
                crossAxisCount: 2,
                shrinkWrap: true,
                crossAxisSpacing: 20,
                mainAxisSpacing: 20,
                children: [
                  LanguageTile(
                    _localizedValues['english'],
                    this.languageList == LanguageList.english ? true : false,
                    () => setLanguage(LanguageList.english),
                  ),
                  LanguageTile(
                    _localizedValues['hindi'],
                    this.languageList == LanguageList.hindi ? true : false,
                    () => setLanguage(LanguageList.hindi),
                  ),
                  LanguageTile(
                    _localizedValues['kannada'],
                    this.languageList == LanguageList.kannada ? true : false,
                    () => setLanguage(LanguageList.kannada),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: PrimaryButton(
                title: "Continue",
                onPress: () => {},
              ),
            )
          ],
        ),
      ),
    );
  }
}
