import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/utils/keyboard_action_config.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

enum ItemsType { goods, services }

class ItemsForm extends StatefulWidget {
  ItemsForm({Key key}) : super(key: key);

  @override
  _ItemsFormState createState() => _ItemsFormState();
}

class _ItemsFormState extends State<ItemsForm> {
  ItemsType itemsType = ItemsType.goods;
  final _formKey = GlobalKey<FormState>();

  bool showImage = false;

  TextEditingController nameTextController = TextEditingController();
  TextEditingController codeTextController = TextEditingController();
  TextEditingController descTextController = TextEditingController();
  TextEditingController hsnTextController = TextEditingController();
  TextEditingController gstRateTextController = TextEditingController();
  TextEditingController rateTextController = TextEditingController();
  TextEditingController unitTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    nameTextController.dispose();
    codeTextController.dispose();
    descTextController.dispose();
    hsnTextController.dispose();
    gstRateTextController.dispose();
    rateTextController.dispose();
    unitTextController.dispose();
    super.dispose();
  }

  void handleImageContainer() {
    setState(() {
      showImage = !this.showImage;
    });
  }

  void setItemsType(ItemsType itemsType) {
    setState(() {
      this.itemsType = itemsType;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ThemeColors.white,
          centerTitle: true,
          title: Text(
            "Add Items",
            style: ThemeText.pageHeaderTitleDark,
          ),
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              MdiIcons.close,
              color: ThemeColors.secondaryDark,
            ),
            onPressed: () => AppRouter.goBack(),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () => {
                  if (_formKey.currentState.validate())
                    AppRouter.goTo(AppRouter.customerForm)
                  else
                    Scaffold.of(context)
                        .showSnackBar(SnackBar(content: Text('Error'))),
                },
                child: Center(
                  child: Text(
                    "save",
                    style: ThemeText.signOutDark,
                  ),
                ),
              ),
            )
          ],
        ),
        body: KeyboardActions(
          disableScroll: true,
          autoScroll: false,
          config: buildConfig(context),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Container(
                        decoration: BoxDecoration(
                          color: ThemeColors.lightBlue,
                          borderRadius: BorderRadius.all(
                            Radius.circular(12),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                child: CustomToggleButtons(
                                  "Goods",
                                  itemsType == ItemsType.goods,
                                  () => setItemsType(ItemsType.goods),
                                ),
                              ),
                              SizedBox(
                                width: 20.0,
                              ),
                              Expanded(
                                child: CustomToggleButtons(
                                  "Services",
                                  itemsType == ItemsType.services,
                                  () => setItemsType(ItemsType.services),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    if (showImage == true)
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 30),
                        child: Center(
                          child: Stack(
                            children: [
                              Container(
                                width: 120,
                                height: 120,
                                decoration: BoxDecoration(
                                  // border:
                                  //     Border.all(color: ThemeColors.secondary),
                                  color: ThemeColors.lightBlue,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(30),
                                  ),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        'https://cdn.pixabay.com/photo/2014/02/27/16/10/tree-276014__340.jpg'),
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xFFd6d6d6),
                                      offset: Offset(2, 2),
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                top: 10,
                                right: 10,
                                child: GestureDetector(
                                  onTap: () => handleImageContainer(),
                                  child: Container(
                                    padding: EdgeInsets.all(2),
                                    decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0xFFd6d6d6),
                                            offset: Offset(2, 2)),
                                      ],
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Icon(
                                      MdiIcons.close,
                                      size: 18,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    else
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 30),
                        child: GestureDetector(
                          onTap: () => handleImageContainer(),
                          child: Center(
                            child: Container(
                              width: 120,
                              height: 120,
                              decoration: BoxDecoration(
                                border:
                                    Border.all(color: ThemeColors.secondary),
                                color: ThemeColors.lightBlue,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(30),
                                ),
                              ),
                              child: Icon(
                                MdiIcons.image,
                                color: ThemeColors.lightGrey,
                                size: 40,
                              ),
                            ),
                          ),
                        ),
                      ),
                    CustomTextInput(
                      labelText: "Name",
                      hint: "Enter Item Name",
                      focusNode: nodeText2,
                      controller: nameTextController,
                      validator: (String value) {
                        return value.isEmpty ? 'Name Can\'t be empty' : null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextInput(
                      labelText: "Code",
                      hint: "Enter Item Code",
                      focusNode: nodeText3,
                      controller: codeTextController,
                      keyboardType: TextInputType.number,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextInput(
                      labelText: "Description",
                      hint: "Write Description",
                      focusNode: nodeText4,
                      controller: descTextController,
                      maxlines: 3,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextInput(
                      labelText: "HSN Code",
                      hint: "Enter HSN Code",
                      focusNode: nodeText5,
                      controller: hsnTextController,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextInput(
                      labelText: "GST Rate (%)",
                      hint: "Enter GST Rate",
                      focusNode: nodeText6,
                      controller: gstRateTextController,
                      keyboardType:
                          TextInputType.numberWithOptions(decimal: true),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextInput(
                      labelText: "Rate",
                      hint: "Enter Rate",
                      focusNode: nodeText7,
                      controller: rateTextController,
                      keyboardType: TextInputType.number,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      validator: (String value) {
                        return value.isEmpty ? 'Rate Can\'t be empty' : null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextInput(
                      labelText: "Unit Type",
                      hint: "Enter unit type",
                      focusNode: nodeText8,
                      controller: unitTextController,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
