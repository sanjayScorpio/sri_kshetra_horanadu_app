import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/quick_action_links.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class DasboardPage extends StatefulWidget {
  DasboardPage({Key key}) : super(key: key);

  @override
  _DasboardPageState createState() => _DasboardPageState();
}

class _DasboardPageState extends State<DasboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: ThemeColors.primary,
        automaticallyImplyLeading: false,
        title: Text(
          "Sri Kshetra Horanadu",
          style: ThemeText.pageHeaderTitleSecondaryDark,
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Icon(
              MdiIcons.bellOutline,
              color: ThemeColors.secondary,
            ),
          ),
        ],
      ),
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            color: ThemeColors.primaryLight,
          ),
          Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                        child: Container(
                            height:
                                (MediaQuery.of(context).size.width - 20) / 2.5,
                            child: Carousel(
                              boxFit: BoxFit.cover,
                              images: [
                                NetworkImage(
                                    'https://oecdenvironmentfocusblog.files.wordpress.com/2020/06/wed-blog-shutterstock_1703194387_low_nwm.jpg?w=640'),
                                NetworkImage(
                                    'https://d1whtlypfis84e.cloudfront.net/guides/wp-content/uploads/2019/07/23090714/nature-1024x682.jpeg'),
                                NetworkImage(
                                    'https://oecdenvironmentfocusblog.files.wordpress.com/2020/06/wed-blog-shutterstock_1703194387_low_nwm.jpg?w=640'),
                                NetworkImage(
                                    'https://d1whtlypfis84e.cloudfront.net/guides/wp-content/uploads/2019/07/23090714/nature-1024x682.jpeg'),
                              ],
                              dotSize: 3.0,
                              dotSpacing: 15.0,
                              dotColor: ThemeColors.lightGrey,
                              indicatorBgPadding: 10.0,
                              dotBgColor: Colors.transparent,
                              borderRadius: true,
                              autoplayDuration: Duration(seconds: 5),
                            )),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
