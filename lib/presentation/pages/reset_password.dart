import 'package:flutter/material.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/utils/keyboard_action_config.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class ResetPassword extends StatelessWidget {
  ResetPassword({Key key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: KeyboardActions(
          disableScroll: true,
          autoScroll: false,
          config: buildConfig(context),
          child: Container(
            color: ThemeColors.white,
            child: Column(
              children: [
                RectangularGraphic(),
                Padding(
                  padding: const EdgeInsets.only(left: 10, top: 30),
                  child: GoBackButton(),
                ),
                Flexible(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 20.0,
                        vertical: 5.0,
                      ),
                      child: Center(
                        child: Form(
                          key: _formKey,
                          child: Container(
                            alignment: Alignment.center,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 40,
                                ),
                                Text(
                                  "Reset Password",
                                  style: ThemeText.authHeader,
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                                Text(
                                  "Enter the new strong password which includes the combination of number (0-9) and alphabets (a-z and A-Z).",
                                  style: ThemeText.generalDescriptionLight,
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                CustomTextInput(
                                  keyboardType: TextInputType.visiblePassword,
                                  hint: "Enter Password",
                                  focusNode: nodeText1,
                                  // showText: true,
                                  validator: (String value) {
                                    return value.isEmpty
                                        ? 'Cannot be empty'
                                        : null;
                                  },
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                CustomTextInput(
                                  hint: "Confirm Password",
                                  keyboardType: TextInputType.visiblePassword,
                                  // showText: true,
                                  focusNode: nodeText2,
                                  validator: (String value) {
                                    return value.isEmpty
                                        ? 'Cannot be empty'
                                        : null;
                                  },
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                PrimaryButton(
                                  title: 'Continue',
                                  onPress: () => {
                                    if (_formKey.currentState.validate())
                                      {
                                        AppRouter.goTo(
                                            AppRouter.otpVerification)
                                      }
                                    else
                                      {
                                        Scaffold.of(context).showSnackBar(
                                            SnackBar(content: Text('Error')))
                                      },
                                  },
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
