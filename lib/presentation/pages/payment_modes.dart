import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/forms/payment_modes.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

Future<dynamic> paymetModesModal({BuildContext context}) {
  return bottomPageModal(
    context: context,
    titleText: "Payment modes",
    isBackEnabled: true,
    isCancelEnabled: true,
    child: PaymentModesForm(),
  );
}
