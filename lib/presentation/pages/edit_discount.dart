import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/forms/discount_type_form.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

Future<dynamic> editDiscountModal({
  BuildContext context,
}) {
  return bottomPageModal(
      context: context,
      titleText: "Discount type",
      isCancelEnabled: true,
      child: DiscountType());
}
