import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/forms/edit_payment_form.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

Future<dynamic> addEditPaymentModeModal({BuildContext context}) {
  
  return bottomPageModal(
    context: context,
    titleText: "Edit Payment mode",
    isBackEnabled: true,
    isCancelEnabled: true,
    child: EditPaymentFrom(),
  );
}
