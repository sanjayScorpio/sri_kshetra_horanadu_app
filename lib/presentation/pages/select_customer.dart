import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

Future<dynamic> selectCustomerModal({
  BuildContext context,
  VoidCallback onChange,
}) {
  TextEditingController searchText = TextEditingController(text: "");

  return bottomPageModal(
    context: context,
    titleText: "Select customers",
    isCancelEnabled: true,
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: GradientSearchBar(
            controller: searchText,
            hint: "search Customer",
            prefixIcon: MdiIcons.magnify,
            suffixIcon: GestureDetector(
              onTap: () => {searchText.clear()},
              child: Icon(
                MdiIcons.close,
                color: ThemeColors.white,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 15,
        ),
        Container(
          height: MediaQuery.of(context).size.height - 200,
          
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                for (var i = 0; i < 10; i++) ...{
                  CustomerCard(),
                  SizedBox(
                    height: 10,
                  ),
                },
              ],
            ),
          ),
        ),
      ],
    ),
  );
}
