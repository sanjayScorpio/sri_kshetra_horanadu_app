import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/edit_discount.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/dialog.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class OrderDetails extends StatefulWidget {
  OrderDetails({Key key}) : super(key: key);

  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  bool showImage = false;
  bool status = false;
  bool isExpanded = false;

  int itemCount = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void handleImageContainer() {
    setState(() {
      showImage = !this.showImage;
    });
  }

  void handleItemRemove() {
    setState(() {
      itemCount -= 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    double textBoxWidth =
        (MediaQuery.of(context).size.width / 2).toDouble() - 40;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ThemeColors.white,
          centerTitle: true,
          title: Text(
            "Order details",
            style: ThemeText.pageHeaderTitleDark,
          ),
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              MdiIcons.close,
              color: ThemeColors.secondaryDark,
            ),
            onPressed: () => AppRouter.goBack(),
          ),
          actions: [
            GestureDetector(
              onTap: () => dialogModal(context: context, title: "Order status"),
              child: Padding(
                padding: const EdgeInsets.only(right: 15.0),
                child: Icon(
                  MdiIcons.truckDeliveryOutline,
                  color: ThemeColors.secondaryDark,
                ),
              ),
            ),
            GestureDetector(
              onTap: () => AppRouter.goTo(AppRouter.invoiceList),
              child: Padding(
                padding: const EdgeInsets.only(right: 15.0),
                child: Icon(
                  MdiIcons.dotsVertical,
                  color: ThemeColors.secondaryDark,
                ),
              ),
            ),
          ],
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    children: [
                      ItemCustomerSection(),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 15,
                          vertical: 10,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                dataItem(
                                  title: "Order date",
                                  value: '12/12/2020',
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                dataItem(
                                  title: "No of items",
                                  value: '16',
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                dataItem(
                                  title: "Order No",
                                  value: '584584',
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                dataItem(
                                  title: "Deliver on",
                                  value: '12/1/2020 5:30 PM',
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      // Row(
                      //   children: [
                      //     Column(
                      //       crossAxisAlignment: CrossAxisAlignment.start,
                      //       children: [
                      //         Text(
                      //           "Delivered",
                      //           style: ThemeText.orderTextDark,
                      //         ),
                      //         SizedBox(
                      //           height: 5,
                      //         ),
                      //         CustomSwitch(
                      //           activeColor: ThemeColors.secondary,
                      //           value: status,
                      //           onChanged: (value) {
                      //             setState(() {
                      //               status = value;
                      //             });
                      //           },
                      //         ),
                      //       ],
                      //     ),
                      //   ],
                      // ),
                      SizedBox(
                        height: 10,
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.symmetric(
                      //     horizontal: 15,
                      //   ),
                      //   child: Row(
                      //     crossAxisAlignment: CrossAxisAlignment.center,
                      //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //     children: [
                      //       Text(
                      //         "Delivered",
                      //         style: ThemeText.orderTextDark,
                      //       ),
                      //       CustomSwitch(
                      //         activeColor: ThemeColors.secondary,
                      //         value: status,
                      //         onChanged: (value) {
                      //           setState(() {
                      //             status = value;
                      //           });
                      //         },
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      SizedBox(
                        height: 10,
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      Theme(
                        data: Theme.of(context)
                            .copyWith(cardColor: Colors.transparent),
                        child: ExpansionPanelList(
                          animationDuration: Duration(milliseconds: 300),
                          elevation: 0,
                          expansionCallback: (int index, bool iSExpanded) {
                            setState(() {
                              isExpanded = !iSExpanded;
                            });
                          },
                          children: [
                            ExpansionPanel(
                              headerBuilder:
                                  (BuildContext context, bool isExpanded) {
                                return Container(
                                  padding: EdgeInsets.only(left: 3),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Invoice",
                                        style: ThemeText.headdingBoldLight,
                                      ),
                                      GestureDetector(
                                        onTap: () => {
                                          Scaffold.of(context).showSnackBar(
                                            SnackBar(
                                              content: Text('Add'),
                                            ),
                                          ),
                                        },
                                        child: Center(
                                          child: GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                itemCount += 1;
                                              });
                                            },
                                            child: Text(
                                              "Add",
                                              style: ThemeText
                                                  .generalButtonTextPrimary,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  alignment: Alignment.centerLeft,
                                );
                              },
                              isExpanded: isExpanded,
                              body: Column(
                                children: [
                                  for (var i = 0; i < itemCount; i++) ...{
                                    Divider(
                                      thickness: 1,
                                    ),
                                    OrderInvoiceCard(
                                      deleteItem: handleItemRemove,
                                    ),
                                  },
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Text(
                          "Items List",
                          style: ThemeText.headdingBoldLight,
                        ),
                        alignment: Alignment.centerLeft,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      for (var i = 0; i < 5; i++) ...[
                        OrderItem(),
                        Divider(
                          thickness: 1,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              decoration: BoxDecoration(
                color: ThemeColors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                    color: ThemeColors.shadowGrey,
                    offset: Offset(2, -5),
                    blurRadius: 30.0,
                  ),
                ],
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: Row(
                      children: [
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15.0),
                                child: Text(
                                  "Total",
                                  style: ThemeText.orderTotalAmount,
                                ),
                              ),
                              Text(
                                "₹5000",
                                style: ThemeText.primaryBold,
                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () => {
                            bottomPageModal(
                              context: context,
                              isTitleRequired: false,
                              isBackEnabled: false,
                              // isCancelEnabled: true,
                              child: totalList(context),
                            )
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Icon(
                              MdiIcons.informationOutline,
                              color: ThemeColors.darkGrey,
                              size: 30,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget dataItem({title, value}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: ThemeText.generalDescriptionLight,
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          value,
          style: ThemeText.orderTextDark,
        ),
      ],
    );
  }

  Widget totalList(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Stack(
            children: [
              Container(
                child: Center(
                  child: Text(
                    "Price break up",
                    style: ThemeText.itemTotalText,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.only(bottom: 20),
                child: GestureDetector(
                  onTap: () => AppRouter.goBack(),
                  child: Icon(
                    MdiIcons.closeCircleOutline,
                    size: 30,
                  ),
                ),
              )
            ],
          ),
          itemTotalRow(
            text: "Sub Total",
            value: "₹ 50,000",
            isBold: true,
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Text(
                    "Discount  (5%)",
                    style: ThemeText.itemTotalText,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  GestureDetector(
                    onTap: () => editDiscountModal(context: context),
                    child: Icon(
                      MdiIcons.pencil,
                      color: ThemeColors.secondaryDark,
                    ),
                  ),
                ],
              ),
              Text(
                "₹ 50,000",
                style: ThemeText.itemTotalText,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 2,
                child: Text(
                  "Misc  (12% GST)",
                  style: ThemeText.itemTotalTextLight,
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                flex: 1,
                child: TextField(
                  style: ThemeText.textInput,
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  textAlign: TextAlign.right,
                  decoration: InputDecoration(
                    isDense: true,
                    prefix: Text("₹  "),
                    hintStyle: TextStyle(
                      color: ThemeColors.lightGrey,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          itemTotalRow(
            text: "CGST",
            value: "₹ 6500",
            isBold: false,
          ),
          SizedBox(
            height: 10,
          ),
          itemTotalRow(
            text: "SGST",
            value: "₹ 6500",
            isBold: false,
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            thickness: 1,
          ),
          itemTotalRow(
            text: "Total",
            value: "₹ 9596500",
            isBold: true,
          ),
        ],
      ),
    );
  }

  Widget itemTotalRow({text, value, isBold}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          text,
          style: isBold == true
              ? ThemeText.itemTotalText
              : ThemeText.itemTotalTextLight,
        ),
        Text(
          value,
          style: isBold == true
              ? ThemeText.itemTotalText
              : ThemeText.itemTotalTextLight,
        ),
      ],
    );
  }
}
