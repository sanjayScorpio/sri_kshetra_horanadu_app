import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/utils/keyboard_action_config.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class SignIn extends StatefulWidget {
  SignIn({Key key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: ThemeColors.white,
          child: Column(
            children: [
              RectangularGraphic(),
              Flexible(
                child: KeyboardActions(
                  disableScroll: true,
                  autoScroll: false,
                  config: buildConfig(context),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20.0,
                      vertical: 5.0,
                    ),
                    child: SingleChildScrollView(
                      child: Container(
                        child: Form(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 40.0,
                              ),
                              Text(
                                "Let's Sign you in",
                                style: ThemeText.authHeader,
                              ),
                              SizedBox(
                                height: 15.0,
                              ),
                              Text(
                                "Enter your Email / Mobile number",
                                style: ThemeText.authSubHeader,
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                "We'll send you a verification code",
                                style: ThemeText.generalDescriptionLight,
                              ),
                              SizedBox(height: 30.0),
                              CustomTextInput(
                                hint: "E-mail / Phone number",
                                focusNode: nodeText1,
                                inputFormatters: <TextInputFormatter>[
                                  // To do validations for email
                                  // FilteringTextInputFormatter.allow(
                                  //   RegExp(
                                  //       r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"),
                                  // )
                                ],
                                validator: (String value) {
                                  return value.isEmpty ? 'Invalid Email' : null;
                                },
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              CustomTextInput(
                                hint: "Password",
                                focusNode: nodeText2,
                                showText: true,
                                validator: (String value) {
                                  return value.isEmpty
                                      ? 'Password required'
                                      : null;
                                },
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              GestureDetector(
                                onTap: () =>
                                    {AppRouter.goTo(AppRouter.forgotPassword)},
                                child: Container(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "Forgot Password ?",
                                    style: ThemeText
                                        .generalDescriptionBoldUnderline,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              PrimaryButton(
                                title: 'Sign In',
                                onPress: () => {
                                  if (_formKey.currentState.validate())
                                    {AppRouter.goTo(AppRouter.otpVerification)}
                                  else
                                    {
                                      Scaffold.of(context).showSnackBar(
                                          SnackBar(content: Text('Error')))
                                    },
                                },
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Center(
                                child: RichText(
                                  text: TextSpan(
                                    text: 'Don\'t have an Account ? ',
                                    style: ThemeText.generalDescriptionLight,
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: 'Register',
                                          style: ThemeText
                                              .generalDescriptionBoldUnderline,
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () => {
                                                  AppRouter.goTo(
                                                      AppRouter.signUp)
                                                }),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Center(
                                  child: Text(
                                    "OR",
                                    style: ThemeText.generalDescriptionBold,
                                  ),
                                ),
                              ),
                              Center(
                                child: GoogleAuthButton(
                                  "Sign in with Google",
                                ),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
