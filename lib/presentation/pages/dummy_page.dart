import 'package:flutter/material.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/add_edit_payment_types.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/journal_entry.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/ledger_entry.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/payment_modes.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/add_invoice.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/add_payment.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/edit_discount.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/order_filters.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/select_customer.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/select_items.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';

class DummyPage extends StatefulWidget {
  DummyPage({Key key}) : super(key: key);

  @override
  _DummyPageState createState() => _DummyPageState();
}

class _DummyPageState extends State<DummyPage> {
  void setItemsType(itemsType) {
    setState(() {
      itemsType = itemsType;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("bottom popup"),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 100,
                      vertical: 10,
                    ),
                    child: Text("Add invoice"),
                  ),
                  color: ThemeColors.primary,
                  textColor: ThemeColors.white,
                  onPressed: () => addInvoiceModal(context: context),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100, vertical: 10),
                    child: Text("Add payment"),
                  ),
                  color: ThemeColors.secondary,
                  textColor: ThemeColors.white,
                  onPressed: () => addPaymentModal(context: context),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100, vertical: 10),
                    child: Text("Select Customer"),
                  ),
                  color: ThemeColors.infoOrange,
                  textColor: ThemeColors.white,
                  onPressed: () => selectCustomerModal(context: context),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100, vertical: 10),
                    child: Text("Select Items"),
                  ),
                  color: ThemeColors.errorRed,
                  textColor: ThemeColors.white,
                  onPressed: () => selectItemsModal(context: context),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100, vertical: 10),
                    child: Text("filters"),
                  ),
                  color: ThemeColors.successGreen,
                  textColor: ThemeColors.white,
                  onPressed: () => orderFiltersModal(context: context),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100, vertical: 10),
                    child: Text("Edit discount"),
                  ),
                  color: ThemeColors.skyBlue,
                  textColor: ThemeColors.white,
                  onPressed: () => editDiscountModal(context: context),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100, vertical: 10),
                    child: Text("Payment types List"),
                  ),
                  color: ThemeColors.safforn,
                  textColor: ThemeColors.white,
                  onPressed: () => paymetModesModal(context: context),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100, vertical: 10),
                    child: Text("Edit Payment types"),
                  ),
                  color: ThemeColors.violet,
                  textColor: ThemeColors.white,
                  onPressed: () => addEditPaymentModeModal(context: context),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100, vertical: 10),
                    child: Text("Journal Entry"),
                  ),
                  color: ThemeColors.darkGrey,
                  textColor: ThemeColors.white,
                  onPressed: () => journalEnteryModal(context: context),
                ),
                RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 100, vertical: 10),
                    child: Text("Ledger types"),
                  ),
                  color: ThemeColors.green,
                  textColor: ThemeColors.white,
                  onPressed: () => ledgerEnteryModal(context: context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
