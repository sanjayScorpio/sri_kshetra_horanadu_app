import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:sri_kshetra_horanadu/application/state/state.dart';
import 'package:sri_kshetra_horanadu/application/view_models/loading_vm.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/colors.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/application/app_localizations.dart';

class Init extends StatelessWidget {
  const Init({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<RootState, LoadingViewModel>(
      converter: LoadingViewModel.fromStore,
      builder: (context, loadingVM) {
        if (loadingVM.appState == AppState.init) {
          Timer(Duration(seconds: 2), loadingVM.initApp);
        }
        return Scaffold(
          body: Container(
            width: double.infinity,
            height: double.infinity,
            color: ThemeColors.secondary,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Column(
                  children: [
                    Expanded(
                      child: Container(
                        color: ThemeColors.secondary,
                      ),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 10,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            // Image.asset('assets/images/logo_header.png'),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text("Sri Kshetra Horanadu",
                                    style: ThemeText.launchScreenTitle,textAlign: TextAlign.center,),
                              ),
                            ),
                            // Text(
                            //   AppLocalizations.of(context)
                            //       .translate('first_string'),
                            //   style: ThemeText.pageHeaderTitleSmallWhite,
                            // )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                if (loadingVM.appState == AppState.loading) ...{
                  Positioned(
                    bottom: 0.0,
                    child: Container(
                        margin: EdgeInsets.only(bottom: 80.0),
                        child: Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.white,
                            strokeWidth: 3,
                          ),
                        )),
                  )
                },
              ],
            ),
          ),
        );
      },
    );
  }
}
