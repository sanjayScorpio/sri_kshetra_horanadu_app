import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/colors.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/primary_button.dart';

class Walkthrough extends StatefulWidget {
  Walkthrough({Key key}) : super(key: key);

  @override
  _WalkthroughState createState() => _WalkthroughState();
}

class _WalkthroughState extends State<Walkthrough> {
  final introKey = GlobalKey<IntroductionScreenState>();

  Widget _pageImage(String imageFile) {
    return Align(
      child: Image.asset(imageFile),
      alignment: Alignment.bottomCenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: IntroductionScreen(
        key: introKey,
        pages: [
          PageViewModel(
            title: "Enhance Productivity",
            body:
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum  has been the industry's standard dummy text  ever since the 1500s.",
            image: Center(
              child: _pageImage("assets/images/page1.png"),
            ),
            decoration: PageDecoration(
              titleTextStyle: ThemeText.walkthroughTitle,
              bodyTextStyle: ThemeText.walkthroughDescription,
            ),
          ),
          PageViewModel(
            title: "Task Management",
            body:
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum  has been the industry's standard dummy text  ever since the 1500s.",
            image: Center(
              child: _pageImage("assets/images/page2.png"),
            ),
            decoration: PageDecoration(
              titleTextStyle: ThemeText.walkthroughTitle,
              bodyTextStyle: ThemeText.walkthroughDescription,
            ),
          ),
          PageViewModel(
            title: "Manage Statistics",
            image: Center(
              child: _pageImage("assets/images/page3.png"),
            ),
            bodyWidget: Column(
              children: [
                Text(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum  has been the industry's standard dummy text  ever since the 1500s.",
                  style: ThemeText.walkthroughDescription,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: PrimaryButton(
                    onPress: () => {},
                    title: "Get Started",
                  ),
                ),
              ],
            ),
          )
        ],
        onDone: () => {},
        showSkipButton: true,
        skipFlex: 0,
        nextFlex: 0,
        skip: const Text('Skip'),
        next: const Text('Next'),
        done: Text(''),
        dotsDecorator: const DotsDecorator(
          size: Size(10.0, 10.0),
          color: Color(0xFFBDBDBD),
          activeColor: ThemeColors.primary,
          activeSize: Size(22.0, 10.0),
          activeShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(40.0)),
          ),
        ),
      ),
    );
  }
}
