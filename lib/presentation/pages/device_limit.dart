import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/styles.dart';
import 'package:sri_kshetra_horanadu/presentation/widgets/widgets.dart';

class DeviceLimit extends StatefulWidget {
  DeviceLimit({Key key}) : super(key: key);

  @override
  _DeviceLimitState createState() => _DeviceLimitState();
}

class _DeviceLimitState extends State<DeviceLimit> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: ThemeColors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 30.0, horizontal: 24.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 40,
                      height: 40,
                      child: Icon(
                        MdiIcons.informationOutline,
                        color: ThemeColors.white,
                        size: 25,
                      ),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: ThemeColors.infoOrange),
                    ),
                    SizedBox(
                      width: 20.0,
                    ),
                    Expanded(
                      child: Text(
                        "Device Limit",
                        style: ThemeText.deviceLimit,
                      ),
                    ),
                    GestureDetector(
                      onTap: () => {AppRouter.goTo(AppRouter.signIn)},
                      child: Text(
                        "Sign out",
                        style: ThemeText.signOutDark,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: 120,
                height: 120,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                        'https://cdn.pixabay.com/photo/2014/02/27/16/10/tree-276014__340.jpg'),
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(40.0),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Randeep Hodda",
                style: ThemeText.profileDisplayText,
              ),
              SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  "You have already signed into 2 devices. Please sign out of any one the devices.",
                  style: ThemeText.generalDescriptionLight,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      DeviceLimitCard(),
                      DeviceLimitCard(),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 15.0),
                child: PrimaryButton(title: "Continue", onPress: () => {}),
              )
            ],
          ),
        ),
      ),
    );
  }
}
