import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/application/state/state.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/accoun_selection.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/add_edit_customer.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/add_edit_items.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/chart.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/create_password.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/customer_view.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/dashboard.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/customer_payment_ledgure.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/device_limit.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/dummy_page.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/edit_order_details.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/forgot_password.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/home.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/invoice_list.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/language_selection.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/init.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/order_details.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/orders_list.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/pending_amount.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/walkthrough.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/otp_verification.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/reset_password.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/signin.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/signup.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/pages.dart';
import 'package:sri_kshetra_horanadu/presentation/pages/workspace_tiles.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/colors.dart';
import 'package:sri_kshetra_horanadu/presentation/styles/theme.dart';
import 'application/reducer/reducer.dart';
import 'package:redux_saga/redux_saga.dart';
import 'application/saga/saga.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'application/app_localizations.dart';

// create the saga middleware
var sagaMiddleware = createSagaMiddleware();
void main() {
  final store = new Store<RootState>(
    rootStateReducer,
    initialState: RootState.init(),
    middleware: [applyMiddleware(sagaMiddleware)],
  );

  sagaMiddleware.setStore(store);

  sagaMiddleware.run(rootSaga);

  // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
  //   systemNavigationBarColor: ThemeColors.primary, // navigation bar color
  //   statusBarColor: ThemeColors.primary,
  // ));

  runApp(
    Slips(
      store: store,
      title: "Slips",
    ),
  );
}

class Slips extends StatelessWidget {
  const Slips({Key key, this.store, this.title}) : super(key: key);

  final Store<RootState> store;
  final String title;

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          Locale('en', 'IN'),
          Locale('hi', 'IN'),
          Locale('kn', 'IN'),
        ],
        localeResolutionCallback: (locale, supportedLocales) {
          for (var supportedLocale in supportedLocales) {
            if (supportedLocale.languageCode == locale.languageCode &&
                supportedLocale.countryCode == locale.countryCode) {
              return supportedLocale;
            }
          }
          return supportedLocales.first;
        },
        theme: lightThemeData,
        onGenerateRoute: (routeSettings) {
          switch (routeSettings.name) {
            case AppRouter.homeScreen:
              return MaterialPageRoute(builder: (context) => Home());
            case AppRouter.languageSelection:
              return MaterialPageRoute(
                builder: (context) => LanguageSelection(),
              );
            case AppRouter.walkthrough:
              return MaterialPageRoute(builder: (context) => Walkthrough());
            case AppRouter.signIn:
              return MaterialPageRoute(
                builder: (context) => SignIn(),
              );
            case AppRouter.signUp:
              return MaterialPageRoute(
                builder: (context) => SignUp(),
              );
            case AppRouter.forgotPassword:
              return MaterialPageRoute(
                builder: (context) => ForgotPassword(),
              );
            case AppRouter.createPassword:
              return MaterialPageRoute(
                builder: (context) => CreatePassword(),
              );
            case AppRouter.resetPassword:
              return MaterialPageRoute(
                builder: (context) => ResetPassword(),
              );
            case AppRouter.otpVerification:
              return MaterialPageRoute(
                builder: (context) => OTPVerification(),
              );
            case AppRouter.deviceLimit:
              return MaterialPageRoute(
                builder: (context) => DeviceLimit(),
              );
            case AppRouter.accountSelection:
              return MaterialPageRoute(
                builder: (context) => AccountSelection(),
              );
            case AppRouter.customerForm:
              return MaterialPageRoute(
                builder: (context) => CustomerForm(),
              );
            case AppRouter.itemsForm:
              return MaterialPageRoute(
                builder: (context) => ItemsForm(),
              );
            case AppRouter.dashboard:
              return MaterialPageRoute(
                builder: (context) => DasboardPage(),
              );
            case AppRouter.customerView:
              return MaterialPageRoute(
                builder: (context) => CustomerView(),
              );

            case AppRouter.dummyPage:
              return MaterialPageRoute(
                builder: (context) => DummyPage(),
              );

            case AppRouter.customerList:
              return MaterialPageRoute(
                builder: (context) => CustomerList(),
              );
            case AppRouter.itemsList:
              return MaterialPageRoute(
                builder: (context) => ItemsList(),
              );
            case AppRouter.customerPaymentLedgure:
              return MaterialPageRoute(
                builder: (context) => CustomerPaymentLedgure(),
              );
            case AppRouter.pendingAmount:
              return MaterialPageRoute(
                builder: (context) => PendingAmount(),
              );
            case AppRouter.editOrderDetails:
              return MaterialPageRoute(
                builder: (context) => EditOrderDetails(),
              );
            case AppRouter.orderList:
              return MaterialPageRoute(
                builder: (context) => OrdersList(),
              );
            case AppRouter.orderDetails:
              return MaterialPageRoute(
                builder: (context) => OrderDetails(),
              );
            case AppRouter.invoiceList:
              return MaterialPageRoute(
                builder: (context) => InvoiceList(),
              );
            case AppRouter.workspace:
              return MaterialPageRoute(
                builder: (context) => WorkSpace(),
              );
            case AppRouter.chart:
              return MaterialPageRoute(
                builder: (context) => ChartPage(),
              );
            default:
              return MaterialPageRoute(builder: (context) => Init());
          }
        },
        title: title,
        debugShowCheckedModeBanner: false,
        navigatorKey: AppRouter.navKey,
        home: SafeArea(
          child: Scaffold(
            body: Init(),
            bottomNavigationBar: BottomNavigationBar(
              items: [
                BottomNavigationBarItem(
                  icon: Icon(
                    MdiIcons.home,
                    color: ThemeColors.secondary,
                  ),
                  label: "Home",
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    MdiIcons.omega,
                    color: ThemeColors.secondary,
                  ),
                  label: "Temple",
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    MdiIcons.formatListText,
                    color: ThemeColors.secondary,
                  ),
                  label: "Sevas",
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    MdiIcons.textBoxOutline,
                    color: ThemeColors.secondary,
                  ),
                  label: "Info",
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    MdiIcons.menu,
                    color: ThemeColors.secondary,
                  ),
                  label: "More",
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
