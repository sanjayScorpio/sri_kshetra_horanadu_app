import 'package:redux/redux.dart';
import 'package:sri_kshetra_horanadu/application/actions/actions.dart';
import 'package:sri_kshetra_horanadu/application/state/state.dart';

class LoadingViewModel {
  final AppState appState;
  final Function initApp;

  LoadingViewModel({this.appState, this.initApp});

  static LoadingViewModel fromStore(Store<RootState> store) {
    return LoadingViewModel(
      appState: store.state.appState,
      initApp: () => store.dispatch(InitApp()),
    );
  }
}
