import 'package:equatable/equatable.dart';

enum AppState { init, loading, loaded }

class RootState extends Equatable {
  final AppState appState;

  RootState({this.appState});

  @override
  List<Object> get props => [
        appState,
      ];

  factory RootState.init() {
    return RootState(appState: AppState.init);
  }
}
