import 'package:flutter/material.dart';

class AppRouter {
  static final navKey = new GlobalKey<NavigatorState>();

  static const initScreen = "/";
  static const homeScreen = "/home";
  static const languageSelection = "/language";
  static const walkthrough = "/walkthrough";
  static const authScreen = "/auth";
  static const signIn = "/signin";
  static const signUp = "/signup";
  static const otpVerification = "/otpverification";
  static const createPassword = "/createpassword";
  static const resetPassword = "/resetpassword";
  static const forgotPassword = "/forgotpassword";
  static const deviceLimit = "/limit";
  static const accountSelection = "/accountselection";
  static const customerForm = "/customerform";
  static const itemsForm = "/itemsform";
  static const dashboard = "/dashboard";
  static const customerView = "/customerview";
  static const dummyPage = "/dummypage";
  static const customerList = "/customers";
  static const itemsList = "/items";
  static const customerPaymentLedgure = "/customerpaymentledgure";
  static const invoiceForm = "/invoiceform";
  static const pendingAmount = "/pedingamount";
  static const editOrderDetails = '/editorderdetails';
  static const orderList = '/orderlist';
  static const invoiceList = '/invoicelist';
  static const orderDetails = '/orderdetails';
  static const workspace = '/workspace';
  static const chart = '/chart';

  static Future<T> goTo<T>(String routeName, {Object arguments}) async {
    return navKey.currentState.pushNamed<T>(routeName, arguments: arguments);
  }

  static void goBack<T>([T data]) async {
    return navKey.currentState.pop(data);
  }
}
