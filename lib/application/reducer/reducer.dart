import 'package:sri_kshetra_horanadu/application/reducer/app_reducer.dart';
import 'package:sri_kshetra_horanadu/application/state/state.dart';

RootState rootStateReducer(RootState state, action) => new RootState(
      appState: appStateReducer(state.appState, action),
    );
