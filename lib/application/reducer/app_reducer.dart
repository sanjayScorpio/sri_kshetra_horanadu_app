import 'package:redux/redux.dart';
import 'package:sri_kshetra_horanadu/application/actions/actions.dart';
import 'package:sri_kshetra_horanadu/application/state/state.dart';

Reducer<AppState> appStateReducer = combineReducers<AppState>([
  TypedReducer<AppState, InitApp>(_initApp),
  TypedReducer<AppState, LoadApp>(_loadApp),
]);

AppState _initApp(AppState state, action) {
  return AppState.loading;
}

AppState _loadApp(AppState state, action) {
  return AppState.loaded;
}
