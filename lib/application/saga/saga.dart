import 'package:redux_saga/redux_saga.dart';
import 'init_app.dart';

// TODO: remove after adding sagas
helloSaga() sync* {
  print('Hello Sagas!');
}

rootSaga() sync* {
  yield All({
    // TODO: remove after adding sagas
    #hello: helloSaga(),
    #init: initApp(),
  });
}
