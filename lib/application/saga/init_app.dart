import 'package:redux_saga/redux_saga.dart';
import 'package:sri_kshetra_horanadu/application/actions/actions.dart';
import 'package:sri_kshetra_horanadu/application/router/router.dart';
import 'package:sri_kshetra_horanadu/utils/utils.dart';

// ignore: todo
//TODO: check current states

initApp() sync* {
  yield Take(pattern: InitApp);

  yield delay(
    Duration(seconds: 2),
  );

  yield Put(LoadApp());
  yield AppRouter.goTo(AppRouter.dashboard);
}
